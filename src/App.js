

import Authform from 'components/AuthForm';
import GAListener from 'components/GAListener';
import { EmptyLayout, LayoutRoute, MainLayout } from 'components/Layout';
import PageSpinner from 'components/PageSpinner';
import AuthPage from 'pages/AuthPage';
import React from 'react';
import SweetAlert from 'sweetalert-react';
import componentQueries from 'react-component-queries';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import './styles/reduction.scss';
import { Button, Form, FormGroup, Input, Label,Col,Alert } from 'reactstrap';
const ForumPage = React.lazy(() => import('pages/ForumPage'));
const CeventPage = React.lazy(() => import('pages/CeventPage'));
const VeventPage = React.lazy(() => import('pages/VeventPage'));
const VclinicPage = React.lazy(() => import('pages/VclinicPage'));
const Vpatients = React.lazy(() => import('pages/Vpatients'));
const CclinicPage = React.lazy(() => import('pages/CclinicPage'));
const AlertPage = React.lazy(() => import('pages/AlertPage'));
const AuthModalPage = React.lazy(() => import('pages/AuthModalPage'));
const BadgePage = React.lazy(() => import('pages/BadgePage'));
const ButtonGroupPage = React.lazy(() => import('pages/ButtonGroupPage'));
const ButtonPage = React.lazy(() => import('pages/ButtonPage'));
const CardPage = React.lazy(() => import('pages/CardPage'));
const ChartPage = React.lazy(() => import('pages/ChartPage'));
const DashboardPage = React.lazy(() => import('pages/DashboardPage'));
const DropdownPage = React.lazy(() => import('pages/DropdownPage'));
const FormPage = React.lazy(() => import('pages/FormPage'));
const InputGroupPage = React.lazy(() => import('pages/InputGroupPage'));
const ModalPage = React.lazy(() => import('pages/ModalPage'));
const ProgressPage = React.lazy(() => import('pages/ProgressPage'));
const TablePage = React.lazy(() => import('pages/TablePage'));
const TypographyPage = React.lazy(() => import('pages/TypographyPage'));
const WidgetPage = React.lazy(() => import('pages/WidgetPage'));

const getBasename = () => {
  return `/${process.env.PUBLIC_URL.split('/').pop()}`;
};

class App extends React.Component {
  constructor() {
    super();
 
    this.state = {
      redirect:true,
      email:'',
      password:'',
      show:false,
      visible:false,
    }
    this.handleSubmit= this.handleSubmit.bind(this);
    // this.onDismiss= this.onDismiss.bind(this);
  }
 

   async handleSubmit()
    {
     if(this.state.email == "admin" && this.state.password == "admin"){
   this.setState({redirect:true})
     }
     else{
      this.setState({redirect:false})
     await this.setState({visible:true})
      console.log(this.state.visible)
      if(this.state.visible){
        // console.log("work")
        setTimeout(
          function() {
            // console.log("Working")
            this.setState({visible:false})
          }
          .bind(this),
          2000
      );
      }
     }
    }
    // onDismiss(){

    
     
    // }
  
  render() {
    return (
      <div>
      { !this.state.redirect ? ( 
        <center>
            
        <Col xl={6} lg={12} md={12} style={{paddingTop:'13%'}}>
      <Form >
        {/* {showLogo && (
          <div className="text-center pb-4">
            <img
              src={logo200Image}
              className="rounded"
              style={{ width: 60, height: 60, cursor: 'pointer' }}
              alt="logo"
              // onClick={onLogoClick}
            />
          </div>
        )} */}
        <p style={{fontSize:'25px',fontWeight:'600',color:'#26292C'}}>Login</p>
        <FormGroup>
          <Label style={{ float: 'left', margin: '1px'}}>Email</Label>
          <Input type="email" placeholder="Email"  onChange={(event) => this.setState({email: event.target.value})} />
        </FormGroup>
        <FormGroup>
          <Label style={{ float: 'left', margin: '1px'}}>Password</Label>
          <Input type="password" placeholder="Password"  onChange={(event) => this.setState({password: event.target.value})}/>
        </FormGroup>
        <Alert color="danger" isOpen={this.state.visible} toggle={this.onDismiss}>
    Incorrect Email or Password
    </Alert>
        {/* {this.isSignup && (
          <FormGroup>
            <Label for={confirmPasswordLabel}>{confirmPasswordLabel}</Label>
            <Input {...confirmPasswordInputProps} />
          </FormGroup>
        )} */}
        <FormGroup check>
          {/* <Label check>
            <Input type="checkbox" />{' '}
            {this.isSignup ? 'Agree the terms and policy' : 'Remember me'}
          </Label> */}
        </FormGroup>
        {/* <hr /> */}
        <Button
          size="lg"
          className="bg-gradient-theme-left border-0"
          block
          onClick={this.handleSubmit}>Submit
          {/* {this.renderButtonText()} */}
        </Button>

        {/* <div className="text-center pt-1">
          <h6>or</h6>
          <h6>
            {this.isSignup ? (
              <a href="#login" onClick={this.changeAuthState(STATE_LOGIN)}>
                Login
              </a>
            ) : (
              <a href="#signup" onClick={this.changeAuthState(STATE_SIGNUP)}>
                Signup
              </a>
            )}
          </h6>
        </div> */}

     
      </Form> 
      </Col>
       </center>
      ) : (
      <BrowserRouter basename={getBasename()}>
        <GAListener>
          <Switch>
            {/* <LayoutRoute
              exact
              path="/login"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_LOGIN} />
              )}
            /> */}
            {/* <LayoutRoute
              exact
              path="/signup"
              layout={EmptyLayout}
              component={props => (
                <AuthPage {...props} authState={STATE_SIGNUP} />
              )}
            /> */}

            <MainLayout breakpoint={this.props.breakpoint}>
              <React.Suspense fallback={<PageSpinner />}>
                <Route exact path="/" component={DashboardPage} />
                <Route exact path="/login" component={AuthPage} />
                <Route exact path="/login-modal" component={AuthModalPage} />
                <Route exact path="/buttons" component={ButtonPage} />
                <Route exact path="/cards" component={CardPage} />
                <Route exact path="/widgets" component={WidgetPage} />
                <Route exact path="/typography" component={TypographyPage} />
                <Route exact path="/alerts" component={AlertPage} />
                <Route exact path="/tables" component={TablePage} />
                <Route exact path="/badges" component={BadgePage} />
                <Route
                  exact
                  path="/button-groups"
                  component={ButtonGroupPage}
                />
                <Route exact path="/dropdowns" component={DropdownPage} />
                <Route exact path="/progress" component={ProgressPage} />
                <Route exact path="/modals" component={ModalPage} />
                <Route exact path="/forms" component={FormPage} />
                <Route exact path="/forum" component={ForumPage} />
                <Route exact path="/cevent" component={CeventPage} />
                <Route exact path="/vevent" component={VeventPage} />
                <Route exact path="/vclinic" component={VclinicPage} />
                <Route exact path="/Vpatients" component={Vpatients} />
                <Route exact path="/cclinic" component={CclinicPage} />
                <Route exact path="/input-groups" component={InputGroupPage} />
                <Route exact path="/charts" component={ChartPage} />
              </React.Suspense>
            </MainLayout>
            
        
          </Switch>
        </GAListener>
      </BrowserRouter>
      )}
      </div>
      );
  }
}

const query = ({ width }) => {
  if (width < 575) {
    return { breakpoint: 'xs' };
  }

  if (576 < width && width < 767) {
    return { breakpoint: 'sm' };
  }

  if (768 < width && width < 991) {
    return { breakpoint: 'md' };
  }

  if (992 < width && width < 1199) {
    return { breakpoint: 'lg' };
  }

  if (width > 1200) {
    return { breakpoint: 'xl' };
  }

  return { breakpoint: 'xs' };
};

export default componentQueries(query)(App);

