import Page from 'components/Page';
import React from 'react';
import axios from 'axios';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  FormText,
  Input,
  Label,
  Row,
  InputGroup,
  InputGroupAddon,
  InputGroupText,

  UncontrolledButtonDropdown,
  DropdownToggle,
  DropdownItem,
  DropdownMenu,
} from 'reactstrap';
import { GoogleComponent } from 'react-google-location' 


const API_KEY ="xyz";
class CeventPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    
      latitude:'',
      longitude:'',
      tags:[],
      prequisites:[],
      title:'',
      startDate:'',
      startTime:'',
      address:'',
      description:'',
      seatavailable:'',
      entryfee:'',
      fee:'',
      eventype:'',
      category:'',
      selectedFile: null,
      place: null,
      
images: [],
imageUrls: [],
message: ''
    };
    this.getMyLocation = this.getMyLocation.bind(this)
  };

  async componentDidMount(){
    this.getMyLocation();
   
  
      // this.setState({ data: json });
  }
  getMyLocation() {
    const location = window.navigator && window.navigator.geolocation
    
    if (location) {
      location.getCurrentPosition((position) => {
        this.setState({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
        })
      }, (error) => {
        this.setState({ latitude: 'Fetching..', longitude: 'Fetching..' })
      })
    }

  }
   showPosition(position) {
     console.log(position.coords.latitude);
     console.log(position.coords.longitude);
    // x.innerHTML = "Latitude: " + position.coords.latitude + 
    // "<br>Longitude: " + position.coords.longitude;
  }
  // getInitialState(){
  //   return{file: []}
  // }
  // _onChange(){
  //   // Assuming only image
  //   var file = this.refs.file.files[0];
  //   var reader = new FileReader();
  //   var url = reader.readAsDataURL(file);
  
  //    reader.onloadend = function (e) {
  //       this.setState({
  //           imgSrc: [reader.result]
  //       })
  //     }.bind(this);
  //   console.log(url) // Would see a path?
  //   // TODO: concat files
  // }
 addcount(){
   this.setState({tags:[...this.state.tags,'']})
 }
 handleChange(e,index){
   this.state.tags[index] = e.target.value;
   this.setState({tags:this.state.tags})
 }
 remcount(index){
this.state.tags.splice(index,1);
console.log(this.state.tags,"Working");
this.setState({tags:this.state.tags})
 }
 preaddcount(){
  this.setState({prequisites:[...this.state.prequisites,'']})
}
prehandleChange(e,index){
  this.state.prequisites[index] = e.target.value;
  this.setState({prequisites:this.state.prequisites})
}
preremcount(index){
this.state.prequisites.splice(index,1);
console.log(this.state.prequisites,"Working");
this.setState({prequisites:this.state.prequisites})
}
 tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
//  postData(url = ``, data = {}) {
//    console.log(JSON.stringify(data));
//   return axios.post(url, {
//       method: "POST",
//       mode: "cors",
//       cache: "no-cache", 
//       credentials: "same-origin",
//       headers: {
//           "Content-Type": "application/json"
//       },
//       redirect: "follow",
//       referrer: "no-referrer",
//       body: data
//   })
//   .then(response => response.json()); // parses response to JSON
// }

 handleSubmitval = async () => {
  // e.preventDefault();
  // const fd = new FormData();
  // fd.append("image", this.state.selectedFile, this.state.selectedFile.name);
        
  const tim=this.tConvert(this.state.startTime);
  // const dat=this.state.startDate.concat('T');
  // const timdat=dat.concat(tim);
  // console.log(this.state.selectedFile);
  // console.log(timdat);

  console.log(this.state.title);
  console.log(this.state.tags);
  console.log(this.state.startDate);
  console.log(tim);
  console.log(this.state.address);
  console.log(this.state.description);
  console.log(this.state.prequisites);
  console.log(this.state.seatavailable);
  console.log(this.state.entryfee);
  console.log(this.state.fee);
  console.log(this.state.category);
  console.log(this.state.eventype);
  console.log(this.state.latitude);
  console.log(this.state.longitude);
  // console.log(data);

//  await axios.post('http://15.206.167.142:3000/event/create', {
//     "title": this.state.title,
//   })
//   .then((response) => {
//     console.log(response);
//   }, (error) => {
//     console.log(error);
//   });
// console.log(fd);


  // var request = new XMLHttpRequest();
  // // POST to httpbin which returns the POST data as JSON
  // request.open('POST', 'http://15.206.167.142:3000/event/create', /* async = */ false);

  // var formData = new FormData(this.state.title,this.state.tags,this.state.address,this.state.description,this.state.prequisites,this.state.seatavailable);
  // request.send(formData);

  // console.log(request.response);

//   var formData = new FormData();
  
  
//   formData.set( 'title',this.state.title);
//   formData.set( 'images',"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850"); //image from firebase storage 
//   formData.set( "tags",this.state.tags);
//   formData.set("address",this.state.address);
//   formData.set("description",this.state.description);
//   formData.set("prequisites",this.state.prequisites);
//   formData.set("seatAvailable",this.state.seatavailable);
//   formData.set("entryfee",this.state.entryfee);
// //"images":"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850", //image from firebase storage 
// formData.set("event_date",this.state.startDate);
// formData.set("fee",this.state.fee);
// formData.set("Category",this.state.category);
// formData.set("event_type",this.state.eventype);
// formData.set("lat",this.state.latitude);
// formData.set("lng",this.state.longitude);
//   // formData.append('avatar', fileField.files[0]);
//   console.log(formData);
  // axios.post('http://15.206.167.142:3000/event/create', {
  //   method: 'POST',
  //   body: formData
  // })
  // .then(response => response.json())
  // .catch(error => console.error('Error:', error))
  // .then(response => console.log('Success:', JSON.stringify(response)))
  // axios.post({
  //   method: 'post',
  //   url: 'http://15.206.167.142:3000/event/create',
  //   data: formData,
  //   headers: {'Content-Type': 'multipart/form-data' }
  //   })
  //   .then(function (response) {
  //       //handle success
  //       console.log(response);
  //   })
  //   .catch(function (response) {
  //       //handle error
  //       console.log(response);
  //   });

//   this.postData(`http://15.206.167.142:3000/event/create`, {  title: this.state.title,
//   images:"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850", //image from firebase storage 
//      tag:this.state.tags,
// address:this.state.address,
// description:this.state.description,
// prequisites:this.state.prequisites,
// seatAvailable:this.state.seatavailable,
// entryfee:this.state.entryfee,
// //"images":"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850", //image from firebase storage 
// event_date:this.state.startDate,
// fee:this.state.fee,
// Category:this.state.category,
// event_type:this.state.eventype,
// lat:this.state.latitude,
// lng:this.state.longitude})
//   .then(data => console.log(JSON.stringify(data)))
//   .catch(error => console.error(error));


  axios.post('http://15.206.167.142:3000/event/create', {
       "title": this.state.title,
   "images":"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850", //image from firebase storage 
      "tags":this.state.tags,
"address":this.state.address,
"description":this.state.description,
"prequisites":this.state.prequisites,
"seatAvailable":this.state.seatavailable,
"entryfee":this.state.entryfee,
//"images":"https://firebasestorage.googleapis.com/v0/b/android-b7591.appspot.com/o/nurse.png?alt=media&token=79b82692-a3d9-4ab1-a637-38b8db0da850", //image from firebase storage 
"event_date":this.state.startDate,
"fee":this.state.fee,
"Category":this.state.category,
"event_type":this.state.eventype,
"lat":this.state.latitude,
"lng":this.state.longitude
      }).then(response =>{
console.log(response);
      }).catch(err =>{
        console.log(err);
      })
     
  
      // console.log(json)
     
    // console.log(' Returned data:', response);
 
  // const =yaxios.post('http://15.206.167.142:3000/event/create', {
  // "title":this.state.title,
  // })
  // .then((response) => {
  //   console.log(response);
  //   console.log(this.state.title);
  // })
  // .catch(function (error) {
  //   console.log(error)
  //   console.log(this.state.title);
  // })
}
handleChangeval = event => {
  this.setState({ title: event.target.value });
  console.log(this.state.title);
};

selectImages = (e) => {
  this.setState({selectedFile:e.target.files[0]});
  }
  render() {
    
    const { latitude, longitude } = this.state
    return (
      <Page
        className="ButtonPage"
        title="Create Event"
        // breadcrumbs={[{ name: 'Verify Doctors', active: true }]}
      >
         <Col xl={8} lg={12} md={12}>
          <Card>
            <CardHeader>Event</CardHeader>
            <CardBody>
              <Form >
             
                <FormGroup>
                  <Label for="exampletitle">Title</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Title"
                    // value={this.state.title}
                
                    onChange={this.handleChangeval}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleimage">Image</Label>
                
                  <Input 
        ref="file" 
        type="file" 
        name="image" 
        onChange={this.selectImages}
       />
        </FormGroup>
 
    {/* Only show first image, for now. */}
    {/* <img src={this.state.imgSrc} /> */}
                {/* </FormGroup> */}
               
                {/* {this.state.tags.map((tags,index)=>{
                  return(
<FormGroup>

                  <Input
                  key={index}
                    type="text"
                    name="tags"
                    placeholder="Tags"
                    value={tags}
                    onChange={(e)=>this.handleChange(e,index)}
                    
              
                  >
                    
                  </Input>
                  <Button onClick={()=>this.remcount(index)} color="danger" active>Remove Tag</Button>
                </FormGroup>

                  )
                })} */}
                   <p>Tags</p>
                <Button onClick={(e)=>this.addcount(e)} color="primary" active>Add Tags</Button>

                <Col md={12}><br/>
                {this.state.tags.map((tags,index)=>{
                  return(
              <InputGroup>
              
              <Input
                  key={index}
                    type="text"
                    name="tags"
                    placeholder="Tags"
                    value={tags}
                    onChange={(e)=>this.handleChange(e,index)}
                                  
                  />
         
                  <Button onClick={()=>this.remcount(index)} color="danger" active>Remove</Button>
             
              </InputGroup>
              
              )
                })}
           
        </Col>

                <FormGroup>
                  <Label for="exampledate">Date</Label>
                  <Input
                    type="date"
                    name="date"
                    id="exampleDate"
                    placeholder="Date"
                    onChange={(event) => this.setState({startDate: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampledate">Time</Label>
                  <Input
                    type="time"
                    name="time"
                    id="exampletime"
                    placeholder="Time"
                    onChange={(event) => this.setState({startTime: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleaddress">Address</Label>
                  <Input type="textarea" name="textaddress" 
                  onChange={(event) => this.setState({address: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampledescription">Description</Label>
                  <Input type="textarea" name="textdescription"
                  onChange={(event) => this.setState({description: event.target.value})}
                  />
                </FormGroup>
                
                {/* {this.state.prequisites.map((prequisites,index)=>{
                  return(
<FormGroup>

                  <Input
                  key={index}
                    type="text"
                    name="prequisites"
                    placeholder="Prequisites"
                    value={prequisites}
                    onChange={(e)=>this.prehandleChange(e,index)}
                    
              
                  >
                    
                  </Input>
                  <Button onClick={()=>this.preremcount(index)} color="danger" active>Remove Prequisites</Button>
                </FormGroup>

                  )
                })} */}
                <p>Prequisites</p>
                <Button onClick={(e)=>this.preaddcount(e)} color="primary" active>Add Prequisites</Button>
                <Col md={12}><br/>
                {this.state.prequisites.map((prequisites,index)=>{
                  return(

<InputGroup>
                  <Input
                  key={index}
                    type="text"
                    name="prequisites"
                    placeholder="Prequisites"
                    value={prequisites}
                    onChange={(e)=>this.prehandleChange(e,index)}
                    
              
                  />
         
                  <Button onClick={()=>this.preremcount(index)} color="danger" active>Remove</Button>
             
              </InputGroup>
              
              )
                })}
           
        </Col>

                <FormGroup>
                  <Label for="exampleavailable">Seat Available</Label>
                  <Input
                    type="text"
                    name="seatavailable"
                    placeholder="Seat Available"
                    onChange={(event) => this.setState({seatavailable: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleentryfee">Entry Fee</Label>
                  <Input
                    type="text"
                    name="entryfee"
                    placeholder="Entry Fee"
                    onChange={(event) => this.setState({entryfee: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="examplefee">Fee</Label>
                  <Input type="select" name="fee"  onChange={(event) => this.setState({fee: event.target.value})}>
                  <option>Select</option>
                    <option>Free</option>
                    <option>Paid</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="examplecategory">Category</Label>
                  <Input type="select" name="category"  onChange={(event) => this.setState({category: event.target.value})}>
                  <option>Select</option>
                    <option>Seminar</option>
                    <option>Symposium</option>
                    <option>Conferences</option>
                    <option>Webinars</option>
                    <option>Workshops</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="exampletype">Event Type</Label>
                  <Input type="select" name="eventype" onChange={(event) => this.setState({eventype: event.target.value})}>
                  <option>Select</option>
                  <option>Laser Dentists</option>
                    <option>Oral and Maxillofacial Surgeon</option>
                    <option>Periodontist</option>
                    <option>Paedodontist</option>
                    <option>Oral Pathologist</option>
                    <option>Prosthodontist</option>
                    <option>Endodontist</option>
                    <option>Oral Medicine {'&'} Radiologist</option>
                    <option>Oral Oncologist</option>
                    <option>Smile Designers</option>
                    <option>Implantologist</option>
                    <option>Clinical Cosmetologist</option>
                    <option>Facial Plastic Surgeons</option>
                    <option>Emergency Medicines</option>
                  
                  </Input>
                </FormGroup>
                <FormGroup>
                <Label for="examplelocation">Location</Label><br />
                  <Label for="exampleentryfee">Latitude</Label>
                  <Input
                    type="text"
                    name="latitude"
                    placeholder="Latitude"
                    value={this.state.latitude} 
                  />
                   <Label for="exampleentryfee">Longitude</Label>
                  <Input
                    type="text"
                    name="longitude"
                    placeholder="Longitude"
                    value={this.state.longitude} 
                  />
                </FormGroup>
                <FormGroup>
                <GoogleComponent
         
         apiKey="AIzaSyBeVt2YDGRyVb-d1g7PCcNbFqzSk76-0vM"
         language={'en'}
         country={'country:in|country:us'}
         coordinates={true}
        //  locationBoxStyle={'custom-style'}
        //  locationListStyle={'custom-style-list'}
         onChange={(e) => { this.setState({ place: e }) }} />
            </FormGroup>
                {/* <FormGroup>
                  <Label for="exampleUrl">Url</Label>
                  <Input
                    type="url"
                    name="url"
                    id="exampleUrl"
                    placeholder="url placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleNumber">Number</Label>
                  <Input
                    type="number"
                    name="number"
                    id="exampleNumber"
                    placeholder="number placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleDatetime">Datetime</Label>
                  <Input
                    type="datetime"
                    name="datetime"
                    id="exampleDatetime"
                    placeholder="datetime placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleDate">Date</Label>
                  <Input
                    type="date"
                    name="date"
                    id="exampleDate"
                    placeholder="date placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleTime">Time</Label>
                  <Input
                    type="time"
                    name="time"
                    id="exampleTime"
                    placeholder="time placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleColor">Color</Label>
                  <Input
                    type="color"
                    name="color"
                    id="exampleColor"
                    placeholder="color placeholder"
                  />
                </FormGroup>
              
                <FormGroup>
                  <Label for="exampleSelectMulti">Select Multiple</Label>
                  <Input type="select" name="selectMulti" multiple>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Input>
                </FormGroup>
               
               
                <FormGroup check>
                  <Label check>
                    <Input type="radio" /> Option one is this and that—be sure
                    to include why it's great
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" /> Check me out
                  </Label>
                </FormGroup> */}
                <center><Button color="primary" onClick={this.handleSubmitval} active>
                Submit
                </Button>
                </center>
              </Form>
            </CardBody>
          </Card>
        </Col>

        
       

      </Page>
    );
  }
}

export default CeventPage;
