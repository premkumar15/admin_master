
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  FormText,
  Input,
  Label,
  CardTitle,
  CardImg,
  ListGroup,
  ListGroupItem,
  CardLink,
  CardText,
  Row,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Collapse
} from 'reactstrap';

import { MdSearch } from 'react-icons/md';

import SweetAlert from 'react-bootstrap-sweetalert';
import ProductMedia from 'components/ProductMedia';
import avatar from 'assets/img/users/user.png';
import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { FaTrashAlt,FaPencilAlt ,} from 'react-icons/fa';
import axios from 'axios';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import { Line } from 'react-chartjs-2';
var title;
class VeventPage extends React.Component {
  constructor() {
    super();
  
    this.state = {
      rSelected: null,
      cSelected: [],
      name:'',
      login:false,
      searchString: '',
      libr:'',
      data:[],
      images:avatar,
      modal: false,
      modal_backdrop: false,
      modal_nested_parent: false,
      modal_nested: false,
      backdrop: true,
      tags:[],
      attendees:'',
      event_date:'Date',
      event_time:'Time',
      event_type:'Event Type',
      Category:'Category',
      fee:'Free',
      isOpen:false,
      showText:false,
      description:'',
      isShown:false,
      sweet:false,
    };
  
    this.name = this.name.bind(this);
     this.toggle_c = this.toggle_c.bind(this);
     this.swe = this.swe.bind(this);
    }
  componentDidMount(){
    this.work();
  }
async work(){
  const options = {
 
  headers: {'x-access-token': process.env.REACT_APP_ACCESS_TOKEN}  
};
  const response = await axios.get('http://15.206.167.142:3000/event/get/list' , options)
    const json = await (response.data.data);
  
    // console.log(json)
    this.setState({ data: json });
    console.log(this.state.data[0].tags)

    this.setState({title:this.state.data[0].title});
    this.setState({images:this.state.data[0].images});
    // // this.setState({location:location});
    this.setState({latitude:this.state.data[0].location.coordinates[0]});
    this.setState({longitude:this.state.data[0].location.coordinates[1]});
    // console.log(tags.length);
    // // this.setState()
    // // {console.log(this.state.tags)}
    // // {console.log(this.state.tags.length)}
    this.setState({tags:this.state.data[0].tags});
    // console.log(this.state.tags)
    // console.log(this.state.tags.length)
    this.setState({entryfee:this.state.data[0].entryfee});
    this.setState({fee:this.state.data[0].fee});
    this.setState({Category:this.state.data[0].Category});
    this.setState({event_type:this.state.data[0].event_type});
    if(this.state.data[0].attendees.length != 0){
    this.setState({attendees:this.state.data[0].attendees});
    }
    else{
      this.setState({attendees:0});
    }
    this.setState({_id:this.state.data[0]._id});
    this.setState({address:this.state.data[0].address});
    this.setState({description:this.state.data[0].description});
    this.setState({seatAvailable:this.state.data[0].seatAvailable});
    this.setState({event_date:this.state.data[0].event_date});
    this.setState({created_at:this.state.data[0].created_at});
    this.setState({updated_at:this.state.data[0].updated_at});
    var input = this.state.event_date;
    
    var fields = input.split('T');
    
    var date_f = fields[0];
    var time_f = fields[1];
    var date_ff=this.formatDate(date_f);
    var fields1 = time_f.split('.');
    var time_ff = fields1[0];
    var time_fff=this.tConvert(time_ff);
    this.setState({event_date:date_ff});
    this.setState({event_time:time_fff});
    var input = this.state.created_at;
    
    var fields = input.split('T');
    
    var date_f = fields[0];
    var time_f = fields[1];
    var date_ff=this.formatDate(date_f);
    var fields1 = time_f.split('.');
    var time_ff = fields1[0];
    var time_fff=this.tConvert(time_ff);
    this.setState({created_at_date:date_ff});
    this.setState({created_at_time:time_fff});
    var input = this.state.updated_at;
    
    var fields = input.split('T');
    
    var date_f = fields[0];
    var time_f = fields[1];
    var date_ff=this.formatDate(date_f);
    var fields1 = time_f.split('.');
    var time_ff = fields1[0];
    var time_fff=this.tConvert(time_ff);
    this.setState({updated_at_date:date_ff});
    this.setState({updated_at_time:time_fff});
// await axios.get('http://15.206.167.142:3000/event/get/list' , options)
//   .then((response) => {
//      title=response.data.data;
//     console.log(response.data.data);
//     console.log(response.status);
//     console.log(response.statusText);
//     console.log(response.headers);
//     console.log(response.config);

   
//   });
//  console.log(title);
}
toggle = modalType => () => {
  if (!modalType) {
    return this.setState({
      modal: !this.state.modal,
    });
  }

  this.setState({
    [`modal_${modalType}`]: !this.state[`modal_${modalType}`],
  });
};
toggle_c(){
  this.setState({isOpen:!this.state.isOpen});
}
onCheckboxBtnClick(selected) {
  const index = this.state.cSelected.indexOf(selected);
  if (index < 0) {
    this.state.cSelected.push(selected);
  } else {
    this.state.cSelected.splice(index, 1);
  }
  this.setState({ cSelected: [...this.state.cSelected] });
}
handleChange = (e) => {
  this.setState({ searchString:e.target.value });
}
formatDate (input) {
  var datePart = input.match(/\d+/g),
  year = datePart[0].substring(2), // get only two digits
  month = datePart[1], day = datePart[2];

  return day+'/'+month+'/'+year;
}
 tConvert (time) {
  // Check correct time format and split into components
  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}
name(title,images,location,tags,entryfee,fee,Category,event_type,attendees,_id,address,description,seatAvailable,event_date,created_at,updated_at ){


  this.setState({title:title});//
this.setState({images:images});//
// this.setState({location:location});
this.setState({latitude:location.coordinates[0]});
this.setState({longitude:location.coordinates[1]});
console.log(tags.length);
// this.setState()
// {console.log(this.state.tags)}
// {console.log(this.state.tags.length)}
this.setState({tags:tags});
console.log(this.state.tags)
console.log(this.state.tags.length)
this.setState({entryfee:entryfee});//
this.setState({fee:fee});//
this.setState({Category:Category});//
this.setState({event_type:event_type});
if(attendees.length != 0){
this.setState({attendees:attendees});
}
else{
  this.setState({attendees:0});
}
this.setState({_id:_id});
this.setState({address:address});
this.setState({description:description});
this.setState({seatAvailable:seatAvailable});
// this.setState({event_date:event_date});
var input = event_date;

var fields = input.split('T');

var date_f = fields[0];
var time_f = fields[1];
var date_ff=this.formatDate(date_f);
var fields1 = time_f.split('.');
var time_ff = fields1[0];
var time_fff=this.tConvert(time_ff);
this.setState({event_date:date_ff});
this.setState({event_time:time_fff});
var input = created_at;

var fields = input.split('T');

var date_f = fields[0];
var time_f = fields[1];
var date_ff=this.formatDate(date_f);
var fields1 = time_f.split('.');
var time_ff = fields1[0];
var time_fff=this.tConvert(time_ff);
this.setState({created_at_date:date_ff});
this.setState({created_at_time:time_fff});
var input = updated_at;

var fields = input.split('T');

var date_f = fields[0];
var time_f = fields[1];
var date_ff=this.formatDate(date_f);
var fields1 = time_f.split('.');
var time_ff = fields1[0];
var time_fff=this.tConvert(time_ff);
this.setState({updated_at_date:date_ff});
this.setState({updated_at_time:time_fff});
// this.setState({attendees:'Nil'});
} 
swe(){
  this.setState({sweet:!this.state.sweet})
}

  render() {
//     const options = {
//       headers: {'x-access-token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7Il9pZCI6IjVlMmJjZTQyNTBmYzJmN2YwNTliNjMzNCJ9LCJpYXQiOjE1Nzk5MjkxNTQsImV4cCI6MTYxMTQ2NTE1NH0.qJaWWRVrO_9nO1zET7cFNNxRbjyE97nSVZ7Qc3F6jms'}
//     };
    
//   axios.get('http://15.206.167.142:3000/event/get/list' , options)
//     .then((response) => {
//       this.setState({libr:response.data.data});
//       console.log(response.data.data);
//       console.log(response.status);
//       console.log(response.statusText);
//       console.log(response.headers);
//       console.log(response.config);
// var libraries=this.state.libr;
// console.log(libraries);
     
//     });
 
    // var libraries = [

    //   { name: 'Mike'},
    //   { name: 'Jack'},
    //   { name: 'John'},
    //   { name: 'David'},
    //   { name: 'Mike'},
    //   { name: 'Jack'},
    //   { name: 'John'},
    //   { name: 'David'},
    
    
    // ];
  //  this.work();
    var  searchString = this.state.searchString.trim().toLowerCase();
    
        //  console.log(this.state.data);
      
     if (searchString.length > 0) {
      //  console.log(searchString)
       this.state.data   = this.state.data.filter(function(i) {
        //  console.log(i);
        //  this.setState({ data: [...this.state.data, libraries ] })
         return i.title.toLowerCase().match( searchString );
       });
     }
     const { title } = this.state;
    return (
      <Page
        className="ButtonPage"
        title="View Events"
        // breadcrumbs={[{ name: 'Verify Doctors', active: true }]}
      >
        {/* <Row>
        <Col md="3" sm="12" xs="12">
        <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <SweetAlert
  danger
  showCancel
  show={this.state.sweet}
  confirmBtnText="Yes, delete it!"
  confirmBtnBsStyle="danger"
  cancelBtnBsStyle="default"
  title="Are you sure want to delete?"
  onConfirm={this.swe}
  onCancel={this.swe}
  focusCancelBtn
>

</SweetAlert>
  
      <Input
      value={this.state.searchString}
       onChange={this.handleChange}
        type="search"
        className="cr-search-form__input"
        placeholder="Search..."
      />
    
      
       
          
    </Form>
        </Col>
        </Row> */}
       
        <Row>
     
          {/* <Col md="4" sm="12" xs="12" >
            <Card className="mb-3" style={{borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}>
              {/* <CardHeader  style={{borderWidth:1,borderColor:'#828282'}}>Doctors</CardHeader> */}
              {/* <CardBody>  */}
              
          <Col md="5" sm="12" xs="12">
            <Card className="mb-3">
            {/* <CardHeader>Doctors List</CardHeader> */}
            
       
             
              <CardBody>
              {/* {console.log(this.state.data)} */}
                {this.state.data.map(
                  ({  title,images,location,tags,entryfee,fee,Category,event_type,attendees,_id,address,description,seatAvailable,event_date,created_at,updated_at }) => (
                    
                    <Col md="12" sm="12" xs="12">
                    <Button onClick={this.name.bind(this,title,images,location,tags,entryfee,fee,Category,event_type,attendees,_id,address,description,seatAvailable,event_date,created_at,updated_at )} style={{background:'white',color:'black',width:'100%'}}>
                    <ProductMedia
                    
                      image={images}
                      title={title}
                      description={Category}
                      // right={right}
                    />
                    </Button>
                    </Col>
                  ),
                )}
              </CardBody>
            </Card>
          </Col>
         {/* <div>  <Button key={index} style={{background:'#4d4d4d',borderColor:'#4d4d4d'}} onClick={this.name.bind(this,value.name)} >{value.name}</Button></div> */}
      
          
{/*             
              </CardBody>
            </Card>
          </Col> */}
          <Col md={7}>
          
          <UserCard
            // avatar={this.state.images}
            // title={this.state.name}
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
            // subtitle="Project Lead"
            // text="Give me a star!"
          
          >
          <Button style={{background:'white',color:'black',width:'98.5%',borderColor:'white',
         }}> 
           <h3 style={{color:'#395ea3',fontSize:'25px'}}>{this.state.fee + ' ' +this.state.Category + ' on'}</h3>
          </Button>
          <Button style={{background:'white',color:'black',width:'10%',position: 'absolute',borderColor:'white',
         right: 55 }} onClick={this.toggle()}> 
          <h3 style={{color:'#0044c2',fontSize:'25px'}}>< FaPencilAlt /></h3>
          </Button>
             <Button style={{background:'white',color:'black',width:'10%',position: 'absolute',borderColor:'white',
          right: 10}} onClick={this.swe}> 
           <h3 style={{color:'#c20000',fontSize:'25px'}}>< FaTrashAlt /></h3>
          </Button>

                        <p style={{color:'black',textAlign:'center',fontWeight:600,fontSize:'20px',textTransform:'uppercase'}}>{this.state.title}</p>
                        <p style={{color:'black',textAlign:'center'}}>{this.state.event_date  +  ' - '  +  this.state.event_time}</p>
                        <p style={{color:'black',textAlign:'center',fontWeight:600}}> {this.state.event_type + ', ' + this.state.Category}</p>
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Category : </span>{this.state.Category}</p> */}
          <center>
            <CardImg top src={this.state.images} style={{width: '100%',maxWidth: '300px',height: '100%',maxHeight:'300px'}}/>
            </center>
            <br/>
            <Col md="12" sm="12" xs="12">
            {this.state.tags.map((item,i) =>
        <Button  onMouseEnter={() => this.setState({isShown: true})}
        onMouseLeave={() => this.setState({isShown: false})} style={{color:'black',textAlign:'justify',borderWidth:2,borderColor:'#5c5c5c',background:'white',borderRadius:25}}  key={i} >#{item}</Button>
        
        )}
           {/* {this.state.isShown && (
        <div style={{color:'black'}}>
        prem
        </div>
      )} */}
        
        </Col>
          <CardBody left>
         
              <Col md="12" sm="12" xs="12">
              {this.state.description.length != 0 ?
       <div>
       {this.state.isOpen == false ?  
       <div>
      <span style={{  display:' block', maxWidth:'500px',margin:'auto',overflow: 'hidden',whiteSpace: 'nowrap',textOverflow: 'ellipsis',color:'black'}}>{this.state.description} </span>
      <span  onClick={this.toggle_c} style={{ marginBottom: '1rem' ,color:'blue'}}>Read More</span>
      </div>
      :
      <span  onClick={this.toggle_c} style={{ marginBottom: '1rem' ,color:'blue'}}>Read Less</span>
      }
      <Collapse isOpen={this.state.isOpen}>
       
          <CardBody style={{color:'black',textAlign:'justify'}}>
      {this.state.description}
          </CardBody>
       
      </Collapse>
    </div>
   :null}
 
   
               {/* <p style={{color:'black'}}> Event Information</p> */}
              {/* <Button style={{background:'white',color:'black',width:'100%'}}> */}

              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Id : </span>{this.state._id}</p> */}
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Type :</span> {this.state.event_type}</p>
              <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Category : </span>{this.state.Category}</p> */}
          
              <span style={{color:'black',textAlign:'left'}}><span style={{fontWeight:600}}>Seat Available : </span>{this.state.seatAvailable}</span>
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Date : </span>{this.state.event_date}</p>
              <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Time : </span>{this.state.event_time}</p> */}
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Fee : </span>{this.state.fee}</p> */}
              <span style={{color:'black',position:'absolute',right:0,}}><span style={{fontWeight:600}}>Entry Fee : </span> {this.state.entryfee}</span>
    
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Address :</span> {this.state.address}</p> */}
              {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Description :</span> </p> */}
              
              {/* <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Attendees List</p> */}
         {/* <Button style={{background:'white',color:'black',width:'100%'}}> */}
         {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Attendees: </span>{this.state.attendees}</p> */}
         {/* <br/>
         {this.state.tags.map((item,i) =>
        <Button style={{color:'black',textAlign:'justify',borderWidth:2,borderColor:'black',background:'white',borderRadius:25}}  key={i} >#{item}</Button>
        )} */}


     
         {/* </Button> */}
         {/* </Col> */}

             

              {/* <span style={{  display:' block',width: '200px',overflow: 'hidden',whiteSpace: 'nowrap',textOverflow: 'ellipsis',}}>{this.state.description} </span><span>Read More</span> */}
              {/* </p> */}
              {/* <p>here is some content.</p>
<p id="extra_content" class="extra_content" style={{display:'none'}}>here is some extra content</p>
<button id="read_more" class="read_more" style={{display:'block'}}>Show More</button> */}
              {/* </Button> */}


              </Col>
           
        </CardBody>
        {/* <CardBody left>
         
         <Col md="12" sm="12" xs="12">
      
                    
          <p style={{color:'black'}}>Tags List</p>
         
       
         <Button style={{background:'white',color:'black',width:'100%'}}>

         {this.state.tags.map((item,i) =>
        <p style={{color:'black',textAlign:'justify'}}  key={i} >{item}</p>
        )}
         </Button>
         </Col>
      
   </CardBody> */}
            {/* <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Tags :</span></p> */}
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Event Location</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
        <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Latitude : </span>{this.state.latitude}</p>
         <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Longitude : </span>{this.state.longitude}</p>
         </Button>
         </Col>
      
   </CardBody>
        {/* <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Event Creation and Updation</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Created Date : </span>{this.state.created_at_date}</p>
         <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Created Time : </span>{this.state.created_at_time}</p>
         <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Updated Date : </span>{this.state.updated_at_date}</p>
         <p style={{color:'black',textAlign:'justify'}}><span style={{fontWeight:'700'}}>Event Updated Time : </span>{this.state.updated_at_time}</p>

       
         </Button>
         </Col>
      
   </CardBody> */}
        <CardBody left>
         
        
      
   </CardBody>
   {/* <center>

   <Button onClick={this.toggle()} color="success">Edit</Button>
   </center> */}
  
             {/* <center>
             
              <p style={{color:'black'}}>{this.state.name}</p>
              <p style={{color:'black'}}>Id : </p>
              <p style={{color:'black'}}>Email : </p>
              <p style={{color:'black'}}>Phone : </p>
              <p style={{color:'black'}}>Clinic : </p>
              <Button color="success">Approve</Button>
              <Button color="danger">Reject</Button>
     </center> */}
     <br/>
          </UserCard>
        </Col>
        </Row>
    
              
                <Modal
                  isOpen={this.state.modal}
                  toggle={this.toggle()}
                  className={this.props.className}>
                  <ModalHeader toggle={this.toggle()}>Edit Event</ModalHeader>
                  <ModalBody>
                  <FormGroup>
                  <Label for="exampletitle">Title</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Title" 
                    value={this.state.title}              
                    onChange={(event) => this.setState({title: event.target.value})}
                  />
                </FormGroup>
                {/* <FormGroup>
                  <Label for="exampletitle">Tags</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Title"               
                    onChange={this.handleChangeval}
                  />
                </FormGroup> */}
                {/* <FormGroup>
                  <Label for="exampledate">Date</Label>
                  <Input
                    type="date"
                    name="date"
                    id="exampleDate"
                    placeholder="Date"
                    value={this.state.event_date}
                    // onChange={(event) => this.setState({startDate: event.target.value})}
                  />
                </FormGroup>
                
                <FormGroup>
                  <Label for="exampledate">Time</Label>
                  <Input
                    type="time"
                    name="time"
                    id="exampletime"
                    placeholder="Time"
                    onChange={(event) => this.setState({startTime: event.target.value})}
                  />
                </FormGroup> */}
                
                <FormGroup>
                  <Label for="exampleaddress">Address</Label>
                  <Input type="textarea" name="textaddress" 
                  value={this.state.address}
                  onChange={(event) => this.setState({address: event.target.value})}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampledescription">Description</Label>
                  <Input type="textarea" name="textdescription"
                    value={this.state.description}
                  onChange={(event) => this.setState({description: event.target.value})}
                  />
                </FormGroup>
                
                <FormGroup>
                  <Label for="exampletitle">Seat Available</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Seat Available"  
                    value={this.state.seatAvailable}             
                    onChange={this.handleChangeval}
                  />
                </FormGroup>
              
                <FormGroup>
                  <Label for="exampletitle">Entry Fee</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Entry Fee" 
                    value={this.state.entryfee}              
                    onChange={this.handleChangeval}
                  />
                </FormGroup>
                
                <FormGroup>
                  <Label for="examplefee">Fee</Label>
                  <Input type="select" name="fee" value={this.state.fee}onChange={(event) => this.setState({fee: event.target.value})}>
                  <option>Select</option>
                    <option>free</option>
                    <option>paid</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="examplecategory">Category</Label>
                  <Input type="select" name="category" value={this.state.Category}onChange={(event) => this.setState({category: event.target.value})}>
                  <option>Select</option>
                    <option>seminar</option>
                    <option>symposium</option>
                    <option>conferences</option>
                    <option>webinars</option>
                    <option>workshops</option>
                  </Input>
                </FormGroup>
                
                <FormGroup>
                  <Label for="exampletype">Event Type</Label>
                  <Input type="select" name="eventype" value={this.state.event_type}onChange={(event) => this.setState({eventype: event.target.value})}>
                  <option>Select</option>
                  <option>Laser Dentists</option>
                    <option>Oral and Maxillofacial Surgeon</option>
                    <option>Periodontist</option>
                    <option>Paedodontist</option>
                    <option>Oral Pathologist</option>
                    <option>Prosthodontist</option>
                    <option>Endodontist</option>
                    <option>Orthodontist</option>
                    <option>Oral Medicine {'&'} Radiologist</option>
                    <option>Oral Oncologist</option>
                    <option>Smile Designers</option>
                    <option>Implantologist</option>
                    <option>Clinical Cosmetologist</option>
                    <option>Facial Plastic Surgeons</option>
                    <option>Emergency Medicines</option>
                  
                  </Input>
                </FormGroup>
                
                <FormGroup>
                  <Label for="exampletitle">Latitude</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Latitude" 
                    value={this.state.latitude}              
                    // onChange={this.handleChangeval}
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampletitle">Longitude</Label>
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Longitude"    
                    value={this.state.longitude}           
                    // onChange={this.handleChangeval}
                  />
                </FormGroup>


                  </ModalBody>
                  <center>
               
                  
                    <Button color="primary" onClick={this.toggle()}>
                     Update
                    </Button>{' '}
                    <Button color="secondary" onClick={this.toggle()}>
                      Cancel
                    </Button>
                  
             
                  </center>
                  <br/>
                </Modal>
             
        </Page>
        //   <Col xl={8} lg={12} md={12}>
        //   <Card>
        //   {/* <CardImg top src={bg18Image} /> */}
        //     <CardHeader>Event Details</CardHeader>
        //     <CardImg top src={bg18Image} />
        //     <CardBody>
        //       <Form>
        //       {/* <Col md={12} sm={6} xs={12} className="mb-3">
        //   <Card>
        //     <CardImg top src={bg18Image} />
        
        //   </Card>
        // </Col> */}
        
        //     <FormGroup row>
        //           <Label for="exampleEmail" sm={3}>
        //           Title
        //           </Label>
        //           <Col sm={9}>
        //             <Input
        //               type="text"
        //               name="title"
        //               placeholder="Title"
        //             />
        //           </Col>
        //         </FormGroup>
           
             
        //         {/* <FormGroup>
        //           <Label for="exampleimage">Image</Label>
        //           <Input type="file" name="image" />
                 
        //         </FormGroup> */}
        //         <FormGroup  row>
        //           <Label for="exampletags" sm={3}>Tags</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="tags"
        //             placeholder="Tags"
        //           />
        //           </Col>
        //         </FormGroup >
        //         <FormGroup row>
        //           <Label for="exampledate" sm={3}>Date</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="date"
        //             name="date"
        //             id="exampleDate"
        //             placeholder="Date"
        //           />
        //           </Col>
        //         </FormGroup >
        //         <FormGroup row>
        //           <Label for="exampleaddress" sm={3}>Address</Label>
        //           <Col sm={9}>
        //           <Input type="textarea" name="textaddress" />
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="exampledescription" sm={3}>Description</Label>
        //           <Col sm={9}>
        //           <Input type="textarea" name="textdescription" />
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="exampleprequisites" sm={3}>Prequisites</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="prequisites"
        //             placeholder="Prequisites"
        //           />
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="exampleavailable" sm={3}>Seat Available</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="seatavailable"
        //             placeholder="Seat Available"
        //           />
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="exampleentryfee" sm={3}>Entry Fee</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="entryfee"
        //             placeholder="Entry Fee"
        //           />
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="examplefee" sm={3}>Fee</Label>
        //           <Col sm={9}>
        //           <Input type="select" name="fee">
        //             <option>Free</option>
        //             <option>Paid</option>
        //           </Input>
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="examplecategory" sm={3}>Category</Label>
        //           <Col sm={9}>
        //           <Input type="select" name="category">
        //             <option>Seminar</option>
        //             <option>Symposium</option>
        //             <option>Conferences</option>
        //             <option>Webinars</option>
        //           </Input>
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //           <Label for="exampletype" sm={3}>Event Type</Label>
        //           <Col sm={9}>
        //           <Input type="select" name="eventype">
        //           <option>Laser Dentists</option>
        //             <option>Oral and Maxillofacial Surgeon</option>
        //             <option>Periodontist</option>
        //             <option>Paedodontist</option>
        //             <option>Oral Pathologist</option>
        //             <option>Prosthodontist</option>
        //             <option>Endodontist</option>
        //             <option>Oral Medicine {'&'} Radiologist</option>
        //             <option>Oral Oncologist</option>
        //             <option>Smile Designers</option>
        //             <option>Implantologist</option>
        //             <option>Clinical Cosmetologist</option>
        //             <option>Facial Plastic Surgeons</option>
        //             <option>Emergency Medicines</option>
                  
        //           </Input>
        //           </Col>
        //         </FormGroup>
        //         <FormGroup row>
        //         <Label for="examplelocation" sm={12}>Location</Label><br />
        //           <Label for="exampleentryfee" sm={3}>Latitude</Label>
        //           <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="latitude"
        //             placeholder="Latitude"
        //           />
        //           </Col>
        //            <Label for="exampleentryfee" sm={3}>Longitude</Label>
        //            <Col sm={9}>
        //           <Input
        //             type="text"
        //             name="longitude"
        //             placeholder="Longitude"
        //           />
        //           </Col>
        //         </FormGroup>
      //           {/* <FormGroup>
      //             <Label for="exampleUrl">Url</Label>
      //             <Input
      //               type="url"
      //               name="url"
      //               id="exampleUrl"
      //               placeholder="url placeholder"
      //             />
      //           </FormGroup>
      //           <FormGroup>
      //             <Label for="exampleNumber">Number</Label>
      //             <Input
      //               type="number"
      //               name="number"
      //               id="exampleNumber"
      //               placeholder="number placeholder"
      //             />
      //           </FormGroup>
      //           <FormGroup>
      //             <Label for="exampleDatetime">Datetime</Label>
      //             <Input
      //               type="datetime"
      //               name="datetime"
      //               id="exampleDatetime"
      //               placeholder="datetime placeholder"
      //             />
      //           </FormGroup>
      //           <FormGroup>
      //             <Label for="exampleDate">Date</Label>
      //             <Input
      //               type="date"
      //               name="date"
      //               id="exampleDate"
      //               placeholder="date placeholder"
      //             />
      //           </FormGroup>
      //           <FormGroup>
      //             <Label for="exampleTime">Time</Label>
      //             <Input
      //               type="time"
      //               name="time"
      //               id="exampleTime"
      //               placeholder="time placeholder"
      //             />
      //           </FormGroup>
      //           <FormGroup>
      //             <Label for="exampleColor">Color</Label>
      //             <Input
      //               type="color"
      //               name="color"
      //               id="exampleColor"
      //               placeholder="color placeholder"
      //             />
      //           </FormGroup>
              
      //           <FormGroup>
      //             <Label for="exampleSelectMulti">Select Multiple</Label>
      //             <Input type="select" name="selectMulti" multiple>
      //               <option>1</option>
      //               <option>2</option>
      //               <option>3</option>
      //               <option>4</option>
      //               <option>5</option>
      //             </Input>
      //           </FormGroup>
               
               
      //           <FormGroup check>
      //             <Label check>
      //               <Input type="radio" /> Option one is this and that—be sure
      //               to include why it's great
      //             </Label>
      //           </FormGroup>
      //           <FormGroup check>
      //             <Label check>
      //               <Input type="checkbox" /> Check me out
      //             </Label>
      //           </FormGroup> */}
      // //           <center><Button color="secondary" active>
      // //           Edit
      // //           </Button>
      // //           </center>
      // //         </Form>
      // //       </CardBody>
      // //     </Card>
      // //   </Col>

        
       

      // // </Page>
        
       


    );
  }
}

export default VeventPage;
