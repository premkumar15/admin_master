import React from 'react';
import user1Image from 'assets/img/users/user.png';
import { UserCard } from 'components/Card';
import { MdSearch } from 'react-icons/md';
import ProductMedia from 'components/ProductMedia';
import avatar from 'assets/img/users/user.png';
import {
  Row,
  Col,
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardSubtitle,
  CardBody,
  CardText,
  Form,
  Input
} from 'reactstrap';
import CclinicPage from './CclinicPage'
import Page from 'components/Page';
import bpage from './ButtonPage.js'
import SearchInput from 'components/SearchInput';
class ButtonGroupPage extends React.Component {
  constructor(props) {
    super(props);
 
    this.state = {
    rSelected: null,
    cSelected: [],
    name:'',
 
    searchString: ''
  };
  this.name = this.name.bind(this);
  }
  componentDidMount(){
    // console.log(this.state.login);
  }
  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }
  handleChange = (e) => {
    this.setState({ searchString:e.target.value });
  }
name(value){
  this.setState({name:value})
console.log(this.state.name)
}
  render() {
   var libraries = [

      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
    
    
    ];
    var  searchString = this.state.searchString.trim().toLowerCase();
         console.log(libraries);
     if (searchString.length > 0) {
       console.log(searchString)
       libraries = libraries.filter(function(i) {
         console.log(i);
         return i.name.toLowerCase().match( searchString );
       });
     }
    const elements = ['Mike', 'John', 'David','Mike', 'John', 'David','Mike', 'John'];
    return (
      <Page
        className="ButtonPage"
        title="View Doctors"
        // breadcrumbs={[{ name: 'Verify Doctors', active: true }]}
      >
    <Row>
        <Col md="3" sm="12" xs="12">
        <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <Input
      value={this.state.searchString}
       onChange={this.handleChange}
        type="search"
        className="cr-search-form__input"
        placeholder="Search..."
      />
    
            {/* {libraries.map(function(i) {
                return <li>{i.name} <a href={i.url}>{i.url}</a></li>;
            }) }  */}
       
          
    </Form>
        </Col>
        </Row>
       
        <Row>
     
          {/* <Col md="4" sm="12" xs="12" >
            <Card className="mb-3" style={{borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}>
              {/* <CardHeader  style={{borderWidth:1,borderColor:'#828282'}}>Doctors</CardHeader> */}
              {/* <CardBody>  */}
              
          <Col md="4" sm="12" xs="12">
            <Card className="mb-3">
            {/* <CardHeader>Doctors List</CardHeader> */}
            
       
             
              <CardBody>
                {libraries.map(
                  ({  name }) => (
                    <Col md="12" sm="12" xs="12">
                    <Button onClick={this.name.bind(this,name)} style={{background:'white',color:'black',width:'100%'}}>
                    <ProductMedia
                      // key={id}
                      image={avatar}
                      title={name}
                      description="+91 8754918843 | 21"
                      // right={right}
                    />
                    </Button>
                    </Col>
                  ),
                )}
              </CardBody>
            </Card>
          </Col>
         {/* <div>  <Button key={index} style={{background:'#4d4d4d',borderColor:'#4d4d4d'}} onClick={this.name.bind(this,value.name)} >{value.name}</Button></div> */}
      
          
{/*             
              </CardBody>
            </Card>
          </Col> */}
          <Col md={4}>
          <UserCard
            avatar={user1Image}
            // title={this.state.name}
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
            // subtitle="Project Lead"
            // text="Give me a star!"
          
          >  
          <CardBody left>
         
              <Col md="12" sm="12" xs="12">
               <p style={{color:'black'}}> Personal Information</p>
              <Button style={{background:'white',color:'black',width:'100%'}}>
              <p style={{color:'black'}}>Name : {this.state.name}</p>
              <p style={{color:'black'}}>Gender : </p>
              <p style={{color:'black'}}>Date of Birth : </p>
              <p style={{color:'black'}}>Password : </p>
              <p style={{color:'black'}}>Role : </p>
              <p style={{color:'black'}}>Clinics : </p>
              <p style={{color:'black'}}>Email : </p>
         <p style={{color:'black'}}>Phone : </p>
              </Button>
              </Col>
           
        </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Assistant Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Name : </p>
         <p style={{color:'black'}}>Phone : </p>
         <p style={{color:'black'}}>Verified : </p>
         </Button>
         </Col>
      
   </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Verification Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Email : </p>
         <p style={{color:'black'}}>Phone : </p>
       
         </Button>
         </Col>
      
   </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Clinic Info</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Name :</p>
         <p style={{color:'black'}}>Location : </p>
         <p style={{color:'black'}}>Verified At : </p>
   
     
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Academics Info</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Year of Experience :</p>
         <p style={{color:'black'}}>Institute : </p>
         <p style={{color:'black'}}>Designation : </p>
   
     
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Thesis Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Name : </p>
         <p style={{color:'black'}}>Duration : </p>
       
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black'}}> Coordinates </p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black'}}>Latitude : </p>
         <p style={{color:'black'}}>Longitude : </p>
         <p style={{color:'black'}}>Address : </p>
         </Button>
         </Col>
      
   </CardBody>
          
     <br/>
          </UserCard>
        </Col>
        </Row>  
      {/* <Row>
        <Col md={6}>
          <Card>
            <CardHeader>Button Groups</CardHeader>
            <CardBody>
              <ButtonGroup className="mr-3 mb-3">
                <Button color="primary">Left</Button>
                <Button color="primary">Middle</Button>
                <Button color="primary">Right</Button>
              </ButtonGroup>

              <ButtonGroup className="mr-3 mb-3">
                <Button>Left</Button>
                <Button>Middle</Button>
                <Button>Right</Button>
              </ButtonGroup>
            </CardBody>
          </Card>
        </Col>

        <Col md={6}>
          <Card>
            <CardHeader>Button Toolbar</CardHeader>
            <CardBody>
              <ButtonToolbar>
                <ButtonGroup className="mr-2">
                  <Button>1</Button>
                  <Button>2</Button>
                  <Button>3</Button>
                  <Button>4</Button>
                </ButtonGroup>
                <ButtonGroup className="mr-2">
                  <Button>5</Button>
                  <Button>6</Button>
                  <Button>7</Button>
                </ButtonGroup>
                <ButtonGroup className="mr-2">
                  <Button>8</Button>
                </ButtonGroup>
              </ButtonToolbar>
            </CardBody>
          </Card>
        </Col>
      </Row>
      <Row>
        <Col md={6}>
          <Card>
            <CardHeader>Nesting</CardHeader>
            <CardBody>
              <ButtonGroup>
                <Button>1</Button>
                <Button>2</Button>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret>Dropdown</DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem>Dropdown Link</DropdownItem>
                    <DropdownItem>Dropdown Link</DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </ButtonGroup>
            </CardBody>
          </Card>
        </Col>

        <Col md={6}>
          <Card>
            <CardHeader>Vertical variation</CardHeader>
            <CardBody>
              <ButtonGroup vertical>
                <Button>1</Button>
                <Button>2</Button>
                <UncontrolledButtonDropdown>
                  <DropdownToggle caret>Dropdown</DropdownToggle>
                  <DropdownMenu>
                    <DropdownItem>Dropdown Link</DropdownItem>
                    <DropdownItem>Dropdown Link</DropdownItem>
                  </DropdownMenu>
                </UncontrolledButtonDropdown>
              </ButtonGroup>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col md={6}>
          <Card>
            <CardHeader>Sizing</CardHeader>
            <CardBody>
              <CardText>Large</CardText>
              <ButtonGroup size="lg">
                <Button>Left</Button>
                <Button>Middle</Button>
                <Button>Right</Button>
              </ButtonGroup>

              <CardText className="mt-3">Default</CardText>
              <ButtonGroup>
                <Button>Left</Button>
                <Button>Middle</Button>
                <Button>Right</Button>
              </ButtonGroup>

              <CardText className="mt-3">Small</CardText>
              <ButtonGroup size="sm">
                <Button>Left</Button>
                <Button>Middle</Button>
                <Button>Right</Button>
              </ButtonGroup>
            </CardBody>
          </Card>
        </Col>
      </Row> */}
    </Page>
  );
};
}

export default ButtonGroupPage;
