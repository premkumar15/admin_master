import Page from 'components/Page';
import user1Image from 'assets/img/users/user.png';
import { UserCard } from 'components/Card';
import { MdSearch } from 'react-icons/md';
import React from 'react';
import ProductMedia from 'components/ProductMedia';
import avatar from 'assets/img/users/user.png';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  FormFeedback,
  FormGroup,
  FormText,

  Label,
  Row,

  Input
} from 'reactstrap';
import SearchInput from 'components/SearchInput';
class Vpatients extends React.Component {
  constructor(props) {
    super(props);
 
    this.state = {
    rSelected: null,
    cSelected: [],
    name:'',
    login:false,
    searchString: ''
  };
  this.name = this.name.bind(this);
  }
  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cSelected: [...this.state.cSelected] });
  }
  handleChange = (e) => {
    this.setState({ searchString:e.target.value });
  }
name(value){
  this.setState({name:value})
console.log(this.state.name)
}
  render() {
   var libraries = [

      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
    
    
    ];
    var  searchString = this.state.searchString.trim().toLowerCase();
         console.log(libraries);
     if (searchString.length > 0) {
       console.log(searchString)
       libraries = libraries.filter(function(i) {
         console.log(i);
         return i.name.toLowerCase().match( searchString );
       });
     }
    const elements = ['Mike', 'John', 'David','Mike', 'John', 'David','Mike', 'John'];
    return (
      <Page
      className="ButtonPage"
      title="View Patients"
      // breadcrumbs={[{ name: 'Verify Doctors', active: true }]}
    >
  <Row>
        <Col md="3" sm="12" xs="12">
        <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <Input
      value={this.state.searchString}
       onChange={this.handleChange}
        type="search"
        className="cr-search-form__input"
        placeholder="Search..."
      />
    
            {/* {libraries.map(function(i) {
                return <li>{i.name} <a href={i.url}>{i.url}</a></li>;
            }) }  */}
       
          
    </Form>
        </Col>
        </Row>
       
        <Row>
     
          {/* <Col md="4" sm="12" xs="12" >
            <Card className="mb-3" style={{borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}>
              {/* <CardHeader  style={{borderWidth:1,borderColor:'#828282'}}>Doctors</CardHeader> */}
              {/* <CardBody>  */}
              
          <Col md="4" sm="12" xs="12">
            <Card className="mb-3">
            {/* <CardHeader>Doctors List</CardHeader> */}
            
       
             
              <CardBody>
                {libraries.map(
                  ({  name }) => (
                    <Col md="12" sm="12" xs="12">
                    <Button onClick={this.name.bind(this,name)} style={{background:'white',color:'black',width:'100%'}}>
                    <ProductMedia
                      // key={id}
                      image={avatar}
                      title={name}
                      description="+91 8754918843 | 21"
                      // right={right}
                    />
                    </Button>
                    </Col>
                  ),
                )}
              </CardBody>
            </Card>
          </Col>
         {/* <div>  <Button key={index} style={{background:'#4d4d4d',borderColor:'#4d4d4d'}} onClick={this.name.bind(this,value.name)} >{value.name}</Button></div> */}
      
          
{/*             
              </CardBody>
            </Card>
          </Col> */}
          <Col md={4}>
          <UserCard
            avatar={user1Image}
            // title={this.state.name}
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
            // subtitle="Project Lead"
            // text="Give me a star!"
          
          >  
          <CardBody left>
         
              <Col md="12" sm="12" xs="12">
               <p style={{color:'black'}}> Personal Information</p>
              <Button style={{background:'white',color:'black',width:'100%'}}>
              <p style={{color:'black'}}>Name : {this.state.name}</p>
              <p style={{color:'black'}}>Mobile Number : </p>
              <p style={{color:'black'}}>Email : </p>
              <p style={{color:'black'}}>Date of Birth : </p>
              <p style={{color:'black'}}>Blood Group : </p>
              <p style={{color:'black'}}>Gender : </p>
              <p style={{color:'black'}}>Country : </p>
              <p style={{color:'black'}}>State : </p>
              <p style={{color:'black'}}>City : </p>
              <p style={{color:'black'}}>Doctor Id : </p>
              <p style={{color:'black'}}>Age : </p>
              <p style={{color:'black'}}>Treatment Id : </p>
              </Button>
              </Col>
           
        </CardBody>
        </UserCard>
        </Col>
     </Row>
     </Page>
      
      
     
           
             
    );
  }
}

export default Vpatients;
