import React from 'react';
import user1Image from 'assets/img/users/user.png';
import { UserCard } from 'components/Card';
import { MdSearch } from 'react-icons/md';
import ProductMedia from 'components/ProductMedia';
import avatar from 'assets/img/users/user.png';
import axios from 'axios'
import classnames from "classnames";

import {
  avatarsData,
  chartjs,
  productsData,
  supportTicketsData,
  todosData,
  userProgressTableData,
} from 'demos/dashboardPage';
import {

  Col,
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardSubtitle,
  CardBody,
  CardText,
  FormGroup,
  Label,
  Form,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
  UncontrolledButtonDropdown,
  Input,
  Nav, NavItem, NavLink, TabContent, TabPane
} from 'reactstrap';
import CclinicPage from './CclinicPage'
import Page from 'components/Page';
import SearchInput from 'components/SearchInput';
class ButtonPage extends React.Component {
  constructor(props) {
    super(props);
 
    this.state = {
    rSelected: null,
    cSelected: [],
    name:'',
    data:[],
    login:false,
    searchString: '',
    activeTab:1,
    plainTabs: 1
  };
  this.name = this.name.bind(this);
  }
  async componentDidMount(){
    const options = {
      headers: {'x-access-token': process.env.REACT_APP_ACCESS_TOKEN} 
    };
    const response = await axios.get('http://15.206.167.142:3000/profile/get/detail' , options)
      const json = await (response.data.data);
    
      console.log("Profile Details",json)
      this.setState({ data: json });
      console.log(this.state.data)
      // const response1 = await axios.get('http://15.206.167.142:3000/profile/pending/request' , options)
      // const json1 = await (response1.data);
    
      // console.log("Pending Requests",json1)
      // this.setState({ data1: json1 });
      // const response2 = await axios.get('http://15.206.167.142:3000/profile/get/resident' , options)
      // const json2 = await (response2.data);
    
      // console.log("Get Resident",json2)
      // this.setState({ data2: json2 });
      // const response3 = await axios.get('http://15.206.167.142:3000/profile/get/associates' , options)
      // const json3 = await (response3.data);
    
      // console.log("Get Associates",json3)
      // this.setState({ data3: json3 });
  }
  toggleNavs = (e, state, index) => {
    e.preventDefault();
    this.setState({
      [state]: index
    });
  };
  onCheckboxBtnClick(selected) {
    const index = this.state.cSelected.indexOf(selected);
    if (index < 0) {
      this.state.cSelected.push(selected);
    } else {
      this.state.cSelected.splice(index, 1);
    }
    this.setState({ cS0elected: [...this.state.cSelected] });
  }
  handleChange = (e) => {
    this.setState({ searchString:e.target.value });
  }
name(value){
  this.setState({name:value})
console.log(this.state.name)
}
tabs(tab){
  if(this.state.activeTab !== tab){
this.setState({activeTab:tab});
  }
}
  render() {
   var libraries = [

      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
      { name: 'Mike'},
      { name: 'Jack'},
      { name: 'John'},
      { name: 'David'},
    
    
    ];
    var  searchString = this.state.searchString.trim().toLowerCase();
         console.log(libraries);
     if (searchString.length > 0) {
       console.log(searchString)
       libraries = libraries.filter(function(i) {
         console.log(i);
         return i.name.toLowerCase().match( searchString );
       });
     }
 
    return (
      <Page
        className="ButtonPage"
        title="Verify Doctors"
        // breadcrumbs={[{ name: 'Verify Doctors', active: true }]}
      >
        {/* <Row>
        <Col md="3" sm="12" xs="12">
        <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <Input
      value={this.state.searchString}
       onChange={this.handleChange}
        type="search"
        className="cr-search-form__input"
        placeholder="Search..."
      />
    
          
       
          
    </Form>
        </Col>
        </Row> */}
       
        <Row>
     
          {/* <Col md="4" sm="12" xs="12" >
            <Card className="mb-3" style={{borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}>
              {/* <CardHeader  style={{borderWidth:1,borderColor:'#828282'}}>Doctors</CardHeader> */}
              {/* <CardBody>  */}
              
              <Col md={10}>
                <Row>              <Col md={2}>
              <FormGroup>
            
                  <Input type="select" name="fee"  onChange={(event) => this.setState({fee: event.target.value})}>
                  <option>Select</option>
                    <option>Email</option>
                    <option>UserName</option>
                  </Input>
                </FormGroup>
                </Col>
                <Col md={5}>
                 <FormGroup>
                
                  <Input
                    type="text"
                    name="tilte"
                    placeholder="Title"
                    // value={this.state.title}
                
                    onChange={this.handleChangeval}
                  />
                </FormGroup>
                </Col>
                <Col md={3}>
                <Button color="primary" onClick={this.handleSubmitval} active>
               Get User Details
                </Button>
                </Col>
                </Row>

          
        </Col>
        <Col md={12}>
                <Row>      
        <Col md="3" sm="12" xs="12">
                    <Button  style={{background:'transparent',color:'black',width:'100%',borderColor:'transparent'}}>
                    <ProductMedia
                      // key={id}
                      image={avatar}
                      title="Name: Prem"
                      description="User Id: 12345"
                      // right={right}
                    />
                    </Button>
                    </Col>
                    <Col md="4" sm="12" xs="12">
                      <div style={{textAlign:'center'}}>
                      <p style={{color:'green'}}>Verified</p>
                      <p>Mobile: 8754918843</p>
                      </div>
                      </Col>
                      {/* <Col md="4" sm="12" xs="12">
                      <div style={{textAlign:'center'}}>
                      <p style={{color:'green'}}>Verified</p>
                      <p>Mobile: 8754918843</p>
                      </div>
                      </Col> */}
                    </Row>
                    </Col>
                    <Col md={12}>
                <Row>  
                <div>
                   
      <Nav tabs>
  
        <NavItem  style={{width:'200px',margin:'auto' ,border:1,borderColor:'black'}}>
        
          <NavLink className={this.state.activeTab == '1' ? 'active' : ''} onClick={() => this.tabs('1')} style={{color:'black',borderWidth:1,borderColor:'black'}}>
           Academics
          </NavLink>
         
        </NavItem>
     
        <NavItem style={{width:'200px',margin:'auto' }}>
          <NavLink className={this.state.activeTab == '2' ? 'active' : ''} onClick={() => this.tabs('2')} style={{color:'black',borderWidth:1,borderColor:'black'}}>
       Thesis
          </NavLink>
        </NavItem>
        <NavItem style={{width:'200px',margin:'auto' }}>
          <NavLink className={this.state.activeTab == '3' ? 'active' : ''} onClick={() => this.tabs('3')} style={{color:'black',borderWidth:1,borderColor:'black'}}>
            Qualifications
          </NavLink>
        </NavItem>
        <NavItem style={{width:'200px',margin:'auto' }}>
          <NavLink className={this.state.activeTab == '4' ? 'active' : ''} onClick={() => this.tabs('4')} style={{color:'black',borderWidth:1,borderColor:'black',}}>
            Assistant
          </NavLink>
        </NavItem>
        <NavItem style={{width:'200px',margin:'auto' }}>
          <NavLink className={this.state.activeTab == '5' ? 'active' : ''} onClick={() => this.tabs('5')} style={{color:'black',borderWidth:1,borderColor:'black'}}>
          Wallet
          </NavLink>
        </NavItem>
      </Nav>
      <TabContent activeTab={this.state.activeTab}>
        <TabPane tabId="1">Academics Content</TabPane>
        <TabPane tabId="2">Thesis Content</TabPane>
        <TabPane tabId="3">Qualifications Content</TabPane>
        <TabPane tabId="4">Assistant Content</TabPane>
        <TabPane tabId="5">Wallet Content</TabPane>
      </TabContent>
    </div>  
                  </Row>
                  </Col>
                  {/* <Row className="justify-content-center">
                  <Col className="mt-5 mt-lg-0" lg="12">
        
            <div className="mb-3">
              <small className="text-uppercase font-weight-bold">
                With text
              </small>
            </div>
            <div className="nav-wrapper">
              <Nav
                className="nav-fill flex-column flex-md-row"
                id="tabs-icons-text"
                pills
                role="tablist"
              >
                <NavItem>
                  <NavLink
                    aria-selected={this.state.plainTabs === 1}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.plainTabs === 1
                    })}
                    onClick={e => this.toggleNavs(e, "plainTabs", 1)}
                    href="#pablo"
                    role="tab"
                    style={{color:'black',borderWidth:1,borderColor:'black',borderRightColor:'transparent'}}>
                  
                    Home
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.plainTabs === 2}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.plainTabs === 2
                    })}
                    onClick={e => this.toggleNavs(e, "plainTabs", 2)}
                    href="#pablo"
                    role="tab"
                    style={{color:'black'}}
                  >
                    Profile
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.plainTabs === 3}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.plainTabs === 3
                    })}
                    onClick={e => this.toggleNavs(e, "plainTabs", 3)}
                    href="#pablo"
                    role="tab"
                    style={{color:'black'}}
                  >
                    Messages
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.plainTabs === 4}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.plainTabs === 4
                    })}
                    onClick={e => this.toggleNavs(e, "plainTabs", 4)}
                    href="#pablo"
                    role="tab"
                    style={{color:'black'}}
                  >
                    Messages
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    aria-selected={this.state.plainTabs === 5}
                    className={classnames("mb-sm-3 mb-md-0", {
                      active: this.state.plainTabs === 5
                    })}
                    onClick={e => this.toggleNavs(e, "plainTabs", 5)}
                    href="#pablo"
                    role="tab"
                    style={{color:'black'}}
                  >
                    Messages
                  </NavLink>
                </NavItem>
              </Nav>
            </div>
            <Card className="shadow">
              <CardBody>
                <TabContent activeTab={"plainTabs" + this.state.plainTabs}>
                  <TabPane tabId="plainTabs1">
                    <p className="description">
                      Raw denim you probably haven't heard of them jean shorts
                      Austin. Nesciunt tofu stumptown aliqua, retro synth master
                      cleanse. Mustache cliche tempor, williamsburg carles vegan
                      helvetica. Reprehenderit butcher retro keffiyeh
                      dreamcatcher synth.
                    </p>
                    <p className="description">
                      Raw denim you probably haven't heard of them jean shorts
                      Austin. Nesciunt tofu stumptown aliqua, retro synth master
                      cleanse.
                    </p>
                  </TabPane>
                  <TabPane tabId="plainTabs2">
                    <p className="description">
                      Cosby sweater eu banh mi, qui irure terry richardson ex
                      squid. Aliquip placeat salvia cillum iphone. Seitan
                      aliquip quis cardigan american apparel, butcher voluptate
                      nisi qui.
                    </p>
                  </TabPane>
                  <TabPane tabId="plainTabs3">
                    <p className="description">
                      Raw denim you probably haven't heard of them jean shorts
                      Austin. Nesciunt tofu stumptown aliqua, retro synth master
                      cleanse. Mustache cliche tempor, williamsburg carles vegan
                      helvetica. Reprehenderit butcher retro keffiyeh
                      dreamcatcher synth.
                    </p>
                  </TabPane>
                  <TabPane tabId="plainTabs4">
                    <p className="description">
                      Raw denim you probably haven't heard of them jean shorts
                      Austin. Nesciunt tofu stumptown aliqua, retro synth master
                      cleanse. Mustache cliche tempor, williamsburg carles vegan
                      helvetica. Reprehenderit butcher retro keffiyeh
                      dreamcatcher synth.
                    </p>
                  </TabPane>
                  <TabPane tabId="plainTabs5">
                    <p className="description">
                      Raw denim you probably haven't heard of them jean shorts
                      Austin. Nesciunt tofu stumptown aliqua, retro synth master
                      cleanse. Mustache cliche tempor, williamsburg carles vegan
                      helvetica. Reprehenderit butcher retro keffiyeh
                      dreamcatcher synth.
                    </p>
                  </TabPane>
                </TabContent>
              </CardBody>
            </Card>
          </Col>
          </Row> */}
          {/* <Col md="4" sm="12" xs="12">
            <Card className="mb-3">
          
            
       
             
              <CardBody>
                {libraries.map(
                  ({  name }) => (
                    <Col md="12" sm="12" xs="12">
                    <Button onClick={this.name.bind(this,name)} style={{background:'white',color:'black',width:'100%'}}>
                    <ProductMedia
                      // key={id}
                      image={avatar}
                      title={name}
                      description="+91 8754918843 | 21"
                      // right={right}
                    />
                    </Button>
                    </Col>
                  ),
                )}
              </CardBody>
            </Card>
          </Col>
     
          <Col md={5}>
          <UserCard
            avatar={user1Image}
       
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
           
          
          >  
          <CardBody left>
         
              <Col md="12" sm="12" xs="12">
               <p style={{color:'black',textAlign:'center'}}> Personal Information</p>
              <Button style={{background:'white',color:'black',width:'100%'}}>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Name :</span> {this.state.name}</p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Gender :</span> </p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Date of Birth :</span> </p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Password :</span> </p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Role :</span> </p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Clinics :</span> </p>
              <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Email :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Phone :</span> </p>
              </Button>
              </Col>
           
        </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Assistant Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Name :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Phone :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Verified :</span> </p>
         </Button>
         </Col>
      
   </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Verification Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Email :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Phone :</span> </p>
       
         </Button>
         </Col>
      
   </CardBody>
        <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Clinic Info</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Name :</span></p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Location :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Verified At :</span> </p>
   
     
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Academics Info</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Year of Experience :</span></p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Institute :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Designation :</span> </p>
   
     
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Thesis Status</p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Name :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Duration :</span> </p>
       
         </Button>
         </Col>
      
   </CardBody>
   <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',textAlign:'center'}}> Coordinates </p>
         <Button style={{background:'white',color:'black',width:'100%'}}>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Latitude :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Longitude :</span> </p>
         <p style={{color:'black',textAlign:'left'}}><span style={{fontWeight:'700'}}>Address :</span> </p>
         </Button>
         </Col>
      
   </CardBody>
             <center>
            
              <Button color="success">Approve</Button>
              <Button color="danger">Reject</Button>
     </center>
     <br/>
          </UserCard>
        </Col> */}

        
     
          {/* <Col md="4" sm="12" xs="12">
            <Card className="mb-3" style={{borderWidth:2,borderColor:'#828282'}}>
              <CardHeader style={{borderWidth:2,borderColor:'#828282'}}>Doctors Profile</CardHeader>
              <CardBody>
    <p>Name : {this.state.name}</p>
              <p>Id : </p>
              <p>Email : </p>
              <p>Phone : </p>
              <p>Clinic : </p>
      <center>
              <Button color="success">Approve</Button>
              <Button color="danger">Reject</Button>
              </center>
              </CardBody>
            </Card>
          </Col> */}

          {/* <Col md="6" sm="12" xs="12">
            <Card className="mb-3">
              <CardHeader>Outline Buttons</CardHeader>
              <CardBody>
                <Button outline color="primary">
                  primary
                </Button>
                <Button outline color="secondary">
                  secondary
                </Button>
                <Button outline color="success">
                  success
                </Button>
                <Button outline color="info">
                  info
                </Button>
                <Button outline color="warning">
                  warning
                </Button>
                <Button outline color="danger">
                  danger
                </Button>
                <Button outline color="link">
                  link
                </Button>
              </CardBody>
            </Card>
          </Col> */}
        </Row>

        {/* <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Different Button Sizes</CardHeader>
              <CardBody>
                <Button color="primary" size="sm">
                  Small Button
                </Button>
                <Button color="secondary" size="sm">
                  Small Button
                </Button>
                <Button color="success" size="sm">
                  Small Button
                </Button>
                <Button color="info" size="sm">
                  Small Button
                </Button>
                <Button color="warning" size="sm">
                  Small Button
                </Button>
                <Button color="danger" size="sm">
                  Small Button
                </Button>
                <Button color="link" size="sm">
                  Small Button
                </Button>
              </CardBody>
              <CardBody>
                <Button color="primary">Normal Button</Button>
                <Button color="secondary">Normal Button</Button>
                <Button color="success">Normal Button</Button>
                <Button color="info">Normal Button</Button>
                <Button color="warning">Normal Button</Button>
                <Button color="danger">Normal Button</Button>
                <Button color="link">Normal Button</Button>
              </CardBody>
              <CardBody>
                <Button color="primary" size="lg">
                  Large Button
                </Button>
                <Button color="secondary" size="lg">
                  Large Button
                </Button>
                <Button color="success" size="lg">
                  Large Button
                </Button>
                <Button color="info" size="lg">
                  Large Button
                </Button>
                <Button color="warning" size="lg">
                  Large Button
                </Button>
                <Button color="danger" size="lg">
                  Large Button
                </Button>
                <Button color="link" size="lg">
                  Large Button
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Block Buttons</CardHeader>
              <CardBody>
                <Row>
                  <Col>
                    <Card body className="mb-3">
                      <Button color="primary" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="secondary" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="success" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="info" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="warning" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="danger" size="sm" block>
                        Small Block Button
                      </Button>
                      <Button color="link" size="sm" block>
                        Small Block Button
                      </Button>
                    </Card>
                  </Col>
                  <Col>
                    <Card body className="mb-3">
                      <Button color="primary" block>
                        Normal Block Button
                      </Button>
                      <Button color="secondary" block>
                        Normal Block Button
                      </Button>
                      <Button color="success" block>
                        Normal Block Button
                      </Button>
                      <Button color="info" block>
                        Normal Block Button
                      </Button>
                      <Button color="warning" block>
                        Normal Block Button
                      </Button>
                      <Button color="danger" block>
                        Normal Block Button
                      </Button>
                      <Button color="link" block>
                        Normal Block Button
                      </Button>
                    </Card>
                  </Col>
                  <Col>
                    <Card body>
                      <Button color="primary" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="secondary" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="success" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="info" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="warning" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="danger" size="lg" block>
                        Large Block Button
                      </Button>
                      <Button color="link" size="lg" block>
                        Large Block Button
                      </Button>
                    </Card>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Button State</CardHeader>
              <CardBody>
                <Button color="primary" active>
                  primary active
                </Button>
                <Button color="secondary" active>
                  secondary active
                </Button>
                <Button color="success" active>
                  success active
                </Button>
                <Button color="info" active>
                  info active
                </Button>
                <Button color="warning" active>
                  warning active
                </Button>
                <Button color="danger" active>
                  danger active
                </Button>
                <Button color="link" active>
                  link active
                </Button>
              </CardBody>
              <CardBody>
                <Button color="primary" disabled>
                  primary disabled
                </Button>
                <Button color="secondary" disabled>
                  secondary disabled
                </Button>
                <Button color="success" disabled>
                  success disabled
                </Button>
                <Button color="info" disabled>
                  info disabled
                </Button>
                <Button color="warning" disabled>
                  warning disabled
                </Button>
                <Button color="danger" disabled>
                  danger disabled
                </Button>
                <Button color="link" disabled>
                  link active
                </Button>
              </CardBody>

              <CardBody>
                <Button color="primary" outline active>
                  primary outline active
                </Button>
                <Button color="secondary" outline active>
                  secondary outline active
                </Button>
                <Button color="success" outline active>
                  success outline active
                </Button>
                <Button color="info" outline active>
                  info outline active
                </Button>
                <Button color="warning" outline active>
                  warning outline active
                </Button>
                <Button color="danger" outline active>
                  danger outline active
                </Button>
                <Button color="link" outline active>
                  link outline active
                </Button>
              </CardBody>
              <CardBody>
                <Button color="primary" outline disabled>
                  primary outline disabled
                </Button>
                <Button color="secondary" outline disabled>
                  secondary outline disabled
                </Button>
                <Button color="success" outline disabled>
                  success outline disabled
                </Button>
                <Button color="info" outline disabled>
                  info outline disabled
                </Button>
                <Button color="warning" outline disabled>
                  warning outline disabled
                </Button>
                <Button color="danger" outline disabled>
                  danger outline disabled
                </Button>
                <Button color="link" outline disabled>
                  link outline active
                </Button>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card className="mb-3">
              <CardHeader>Checkbox and Radio Buttons</CardHeader>
              <CardBody>
                <CardSubtitle>Radio Buttons</CardSubtitle>
                <ButtonGroup>
                  <Button
                    color="primary"
                    onClick={() => this.setState({ rSelected: 1 })}
                    active={this.state.rSelected === 1}
                  >
                    One
                  </Button>
                  <Button
                    color="primary"
                    onClick={() => this.setState({ rSelected: 2 })}
                    active={this.state.rSelected === 2}
                  >
                    Two
                  </Button>
                  <Button
                    color="primary"
                    onClick={() => this.setState({ rSelected: 3 })}
                    active={this.state.rSelected === 3}
                  >
                    Three
                  </Button>
                </ButtonGroup>
                <CardText>Selected: {this.state.rSelected}</CardText>
              </CardBody>
              <CardBody>
                <CardSubtitle>Checkbox Buttons</CardSubtitle>
                <ButtonGroup>
                  <Button
                    color="primary"
                    onClick={() => this.onCheckboxBtnClick(1)}
                    active={this.state.cSelected.includes(1)}
                  >
                    One
                  </Button>
                  <Button
                    color="primary"
                    onClick={() => this.onCheckboxBtnClick(2)}
                    active={this.state.cSelected.includes(2)}
                  >
                    Two
                  </Button>
                  <Button
                    color="primary"
                    onClick={() => this.onCheckboxBtnClick(3)}
                    active={this.state.cSelected.includes(3)}
                  >
                    Three
                  </Button>
                </ButtonGroup>
                <CardText>
                  Selected: {JSON.stringify(this.state.cSelected)}
                </CardText>
              </CardBody>
            </Card>
          </Col>
        </Row> */}
      </Page>
    );
  }
}

export default ButtonPage;
