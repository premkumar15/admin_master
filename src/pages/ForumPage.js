
import {
 Button,
  Card,
  CardBody,
  CardImg,
  CardImgOverlay,
  CardLink,
  CardText,
  CardTitle,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
  CardHeader,
  Media,
  UncontrolledTooltip,
  Badge,
  Popover, PopoverHeader, PopoverBody 
} from 'reactstrap';
import {
  avatarsData,
  chartjs,
  productsData,
  supportTicketsData,
  todosData,
  userProgressTableData,
} from 'demos/dashboardPage';
import Spacer from 'react-add-space';
import { Comment, Header } from 'semantic-ui-react'
import { FaCommentDots,FaThumbsUp ,} from 'react-icons/fa';
import Avatar from '../components/Avatar.js';
import avatar from 'assets/img/users/user.png';
import SupportTicket from 'components/SupportTicket';
import bg11Image from 'assets/img/bg/background_1920-11.jpg';
import bg18Image from 'assets/img/bg/background_1920-18.jpg';
import bg1Image from 'assets/img/bg/background_640-1.jpg';
import bg3Image from 'assets/img/bg/background_640-3.jpg';
import user1Image from 'assets/img/users/100_1.jpg';
import { UserCard } from 'components/Card';
import Page from 'components/Page';
import { bgCards, gradientCards, overlayCards } from 'demos/cardPage';
import { getStackLineChart, stackLineChartOptions } from 'demos/chartjs';
import React from 'react';
import axios from 'axios';
import { Line } from 'react-chartjs-2';


class ForumPage extends React.Component {
  constructor() {
    super();
    this.state = {
    
      data:[],
      data1:[],
      data2:[],
      images:avatar,
      id:'',
      like:'',
      toggle:false,
      popoverOpen: false,
      toggle1:false,
      event_date:'',
    }
    this.id = this.id.bind(this);
    this.toggle_pop = this.toggle_pop.bind(this);
    this.like = this.like.bind(this);

  
  };
  async componentDidMount(){
    const options = {
     
     headers: {'x-access-token': process.env.REACT_APP_ACCESS_TOKEN}
    };
    const response = await axios.get('http://15.206.167.142:3000/forum/get/list' , options)
      const json = await (response.data.data);
    
      console.log(json)
      this.setState({ data: json });

        const responsee = await axios.get('http://192.168.4.50:8080/get_sub_occupations')
        .then(res =>
          {
            console.log(res);
          }).catch(err =>{
            console.log(err);
          })
      // const jsone = await (responsee.data);
    
      // console.log(jsone)
      // this.setState({ data: json });
        // const response2 = await axios.get('http://15.206.167.142:3000/forum/like/list/5e3686af5f94ec6ce84e7cd6' , options)
        // const json2 = await (response2.data.data);
      
        // console.log(json2)
        // this.setState({ data2: json2 });
        
  
  }
 async id(value){
 
    this.setState({id:value})
  console.log(this.state.id);
  const options = {
    headers: {'x-access-token': process.env.REACT_APP_ACCESS_TOKEN} 

};
  
  const response1 = await axios.get('http://15.206.167.142:3000/forum/comment/list/'+ value , options)
  const json1 = await (response1.data.data);

  console.log(json1)
  this.setState({ data1: json1 });
 if(this.state.data1.length != 0)
 {
  this.setState({toggle1:!this.state.toggle1})
 }
  //f()
  }
  async like(value1){
  this.toggle_pop();
    this.setState({like:value1})
  console.log(this.state.like);
  const options = {
    headers: {'x-access-token': process.env.REACT_APP_ACCESS_TOKEN} 

  };
  
  const response2 = await axios.get('http://15.206.167.142:3000/forum/like/list/'+ value1 , options)
  const json2 = await (response2.data.data);
  console.log(json2);
  this.setState({ data2: json2 });
   if(this.state.data2.length != 0)
 {
  this.setState({toggle:!this.state.toggle})
 }

  //f()
  }
  toggle_pop() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }
  formatDate (input) {
    var datePart = input.match(/\d+/g),
    year = datePart[0].substring(2), // get only two digits
    month = datePart[1], day = datePart[2];
  
    return day+'/'+month+'/'+year;
  }
  da(date){
    console.log("isdcb");
    var input = date;
console.log(date);
    var fields = input.split('T');
    
    var date_f = fields[0];
    var time_f = fields[1];
    var date_ff=this.formatDate(date_f);
    this.setState({event_date:date_ff});
  }

  render() {
  return (
    <Page title="Forum" 
    // style={{overflow:'hidden',height:'100%',position:'fixed',padding:'80px'}}
    // breadcrumbs={[{ name: 'Forms', active: true }]}
    >
     
       <Row>
       {/* <Col lg="1" md="12" sm="12" xs="12">
         </Col> */}
                 {/* {this.state.data.map(
                  ({ creatorName,id,likes,comments,question,answer,imageUrl }) => (
          <Col md={6} sm={6} xs={12} className="mb-3">
          <Card className="flex-row">
            <CardImg
              className="card-img-left"
              src={imageUrl}
              style={{   width: '100%',maxWidth: '200px',height: '100%',maxHeight: '200px'}}
            />
            <CardBody>
              <CardTitle>Horizontal Image Card(Left)</CardTitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
            </CardBody>
          </Card>
        </Col>
                  ))} */}
        {this.state.data.map(
                  ({ creatorName,id,likes,comments,question,answer,imageUrl }) => (
                    <Col lg="6" md="12" sm="12" xs="12">
                    
                   
            <Card >
              {/* <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                   <span>Forum Data</span> 
                   <Button>
                    <small>View All</small>
                  </Button> 
                </div>
              </CardHeader> */}
              <CardBody>
           
            
     
         
      
        {/* <Media left className="mr-2">
        <Avatar src={avatar} /> */}
         
            <CardImg top src={imageUrl} style={{width: '100%',maxWidth: '500px',height: '100%',maxHeight:'250px'}}/>
        
            {/* </Media> */}
            
            <Media className="m-2">
        <Media body>
          <Media heading tag="h6" className="m-0" style={{}}>
{creatorName}          </Media>
          {/* <p className="text-muted m-0">
      <small style={{opacity:0.6}}>{id}</small>
          </p> */}
        </Media>
      
        <Media right className="align-self-center">
       
        <a 
           style={{color:'#03865F',background:'white',borderColor:'white',}}
            // className={`text-capitalize text-${statusMap[status]}`}
            // id="UncontrolledTooltipExample"  
            onClick={this.like.bind(this,id)}> 
           <h3 style={{color:'#04B883',fontSize:'24px'}}>< FaThumbsUp />{'  '}<sup><Badge color="secondary"style={{fontSize:'12px'}}>{likes} Likes</Badge></sup></h3>
          </a>
          {/* {this.state.like == id ?
          <div>  */}
          {/* <Button  id="Popover1" 
           style={{color:'#03865F',background:'white'}}
         
            onClick={this.like.bind(this,id)} >
            
  Likes <Badge color="secondary">{likes}</Badge>
         
          </Button> */}
         
          {/* <Card> */}
         
      
   

    {/* <p style={{color:'black',fontWeight:'700',textAlign:'center'}}> Like Information</p> */}
  
    {/* <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Creator Name :</span> {creatorName}</p>
  
    <p style={{color:'black'}}><span style={{fontWeight:'700'}}>Created Date :</span> {created_at}</p>

    <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Forum Id :</span> {forumId}</p> */}

    {/* <Popover placement="bottom" isOpen={this.state.popoverOpen} target="Popover1" toggle={this.toggle_pop}>
    {this.state.data2.map(
            ({ creatorName }) => (
  <PopoverHeader>{creatorName}{console.log({creatorName})}</PopoverHeader>
  ),
  )}
        
        </Popover>

   

         </Card>
         </div>
        :null}
        </Media> */}
       
      </Media>
</Media>
        
     
<Media right className="align-self-center">
      {this.state.like == id ? 
      

 <Col lg="12" md="12" sm="12" xs="12">

    
            {this.state.data2.map((item,i) =>
    
        <Button style={{color:'black',textAlign:'justify',borderWidth:2,borderColor:'#5c5c5c',background:'white',borderRadius:25,marginLeft:5,fontSize: '1em',marginTop:5}}  key={i} >< FaThumbsUp style={{color:'#04B883'}} /><sup><Badge style={{fontSize:'8px'}} color="primary">{i+1}</Badge></sup>{' '}{item.creatorName}</Button>
    
        )}
         

    
           
          </Col>
          :null}
          
 </Media><br/>
      <Media>
      <p className="text-muted"><p style={{fontWeight:'500'}}>{question}</p><p style={{textAlign:'justify',color:'#2867bf'}}>{answer}</p></p>
      </Media>
      <Media left className="align-self-center">

          <a  id="Popover1" 
           style={{color:'#03865F',background:'white',borderColor:'white',}}
            // className={`text-capitalize text-${statusMap[status]}`}
            // id="UncontrolledTooltipExample"  
            onClick={this.id.bind(this,id)} >
           <h3 style={{fontSize:'14px',color:'#6A82FB'}} >< FaCommentDots style={{fontSize:'25px'}} />{'  '}<sup><Badge style={{fontSize:'12px'}} color="primary">{comments}</Badge></sup> View Comments</h3>
          </a>
               

          {/* <Button
            style={{color:'#e30000',background:'white',borderColor:'#e30000'}}
            // className={`text-capitalize text-${statusMap[status]}`}
         onClick={this.id.bind(this,id)} 
          >
           Comments <Badge color="danger">{comments}</Badge>
    
          </Button> */}
          
        </Media>
     
        <Media right className="align-self-center">
      {this.state.id == id ? 
      

 <Col lg="12" md="12" sm="12" xs="12" style={{background:'#F8F9FA'}}>

    
            {this.state.data1.map((item,i) =>
         <Comment.Group>
         {/* <Header as='h3' dividing>
           Comments
         </Header> */}
     
         <Comment>
           {/* <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/matt.jpg' /> */}
           <Comment.Content>
             {/* <Comment.Author as='a'>Matt<span>j jbjb</span></Comment.Author>
             <Comment.Metadata>
               <div>Today at 5:42PM</div>
             </Comment.Metadata> */}
        {/* {this.da(item.created_at)}  */}
             <span style={{fontSize:'18px'}} key={i}>    {item.creatorName}{' '}<span class="text-muted" style={{fontSize:'12px'}}>{this.state.event_date}</span></span><br/>
            <span class="text-muted"  key={i}><Spacer amount={8} />< FaCommentDots style={{color:'#6A82FB'}} /><sup><Badge style={{fontSize:'8px'}} color="secondary">{i+1}</Badge></sup>{' '}{item.comment}</span>
         <br/>
           </Comment.Content>
         </Comment>
         </Comment.Group>
           // <Button style={{color:'black',textAlign:'justify',borderWidth:2,borderColor:'#F8F9FA',background:'#F8F9FA',borderRadius:2}}  key={i} >< FaCommentDots style={{color:'#6A82FB'}} /><sup><Badge style={{fontSize:'8px'}} color="secondary">{i+1}</Badge></sup>{' '}{item.comment}</Button>

        )}
         

    
           
          </Col>
          :null}
          
 </Media><br/>

{/* 
    <Comment>
      <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/elliot.jpg' />
      <Comment.Content>
        <Comment.Author as='a'>Elliot Fu</Comment.Author>
        <Comment.Metadata>
          <div>Yesterday at 12:30AM</div>
        </Comment.Metadata>
        <Comment.Text>
          <p>This has been very useful for my research. Thanks as well!</p>
        </Comment.Text>
    
      </Comment.Content>
      <Comment.Group>
        <Comment>
          <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/jenny.jpg' />
          <Comment.Content>
            <Comment.Author as='a'>Jenny Hess</Comment.Author>
            <Comment.Metadata>
              <div>Just now</div>
            </Comment.Metadata>
            <Comment.Text>Elliot you are always so right :)</Comment.Text>
           
          </Comment.Content>
        </Comment>
      </Comment.Group>
    </Comment>

    <Comment>
      <Comment.Avatar src='https://react.semantic-ui.com/images/avatar/small/joe.jpg' />
      <Comment.Content>
        <Comment.Author as='a'>Joe Henderson</Comment.Author>
        <Comment.Metadata>
          <div>5 days ago</div>
        </Comment.Metadata>
        <Comment.Text>Dude, this is awesome. Thanks so much</Comment.Text>
       
      </Comment.Content>
    </Comment> */}

    {/* <Form reply>
      <Form.TextArea />
      <Button content='Add Reply' labelPosition='left' icon='edit' primary />
    </Form> */}

          {/* <p className="text-muted m-0">
      <small style={{opacity:0.6}}>Created Date: {created_at}</small>
          </p> */}

        {/* <Media>
{this.state.toggle == true ?  */}
        {/* <Media>
        {this.state.id == id ? 
        <Col lg="12" md="12" sm="12" xs="12">

{this.state.data1.map(
                  ({ creatorName,forumId,comment,postedBy,commentId,created_at }) => (
                
            <Card >
          
              <CardBody>
           
            <CardBody left>
         
         <Col md="12" sm="12" xs="12">
          <p style={{color:'black',fontWeight:'700',textAlign:'center'}}> Comment Information</p>
        
          <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Creator Name :</span> {creatorName}</p>
          <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Comment :</span> {comment}</p>
          <p style={{color:'black'}}><span style={{fontWeight:'700'}}>Created Date :</span> {created_at}</p>
          <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Posted By :</span> {postedBy}</p>
          <p style={{color:'black'}}><span style={{fontWeight:'700'}}>Comment Id :</span> {commentId}</p>
          <p style={{color:'black',}}><span style={{fontWeight:'700'}}>Forum Id :</span> {forumId}</p>
   
         </Col>
      
   </CardBody>
       

     
 
             
              </CardBody>
            </Card>
          
            ),
            )}
        
  </Col>
  :null}
        </Media> */}
          {/* :null}
          </Media> */}
        
 
             
              </CardBody>
            </Card>
          
   
          </Col>
            ),
            )}
          
          {/* <Col lg="6" md="12" sm="12" xs="12">
            Prem
            </Col> */}
          </Row>       
       
          {/* <UserCard
            avatar={user1Image}
            // title={this.state.name}
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
            // subtitle="Project Lead"
            // text="Give me a star!"
          
          >   */}
      
       
        
    
              {/* <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                   <span>Forum Data</span> 
                   <Button>
                    <small>View All</small>
                  </Button> 
                </div>
              </CardHeader> */}
        
           
            
     
         
      
        {/* <Media left className="mr-2">
        <Avatar src={avatar} /> */}
          {/* <center>
            <CardImg top src={imageUrl} style={{width: '100%',maxWidth: '500px',height: '100%',maxHeight:'250px'}}/>
            </center> */}
            {/* </Media> */}
            
         

          {/* <UserCard
            avatar={user1Image}
            // title={this.state.name}
            style={{background:'#fff',borderWidth:1,borderColor:'#CED4DA',borderRadius:3}}
            // subtitle="Project Lead"
            // text="Give me a star!"
          
          >   */}
      
     
              
       {/* {this.state.data.map({supportTicket}) => (
       <Col lg="4" md="12" sm="12" xs="12">
            <Card>
              <CardHeader>
                <div className="d-flex justify-content-between align-items-center">
                   <span>Forum Data</span> 
                   <Button>
                    <small>View All</small>
                  </Button> 
                </div>
              </CardHeader>
              <CardBody>
           
              <Media className="m-2">
        <Media left className="mr-2">
          <Avatar src={avatar} />
        </Media>
        <Media body>
          <Media heading tag="h6" className="m-0">
           Name
          </Media>
          <p className="text-muted m-0">
            <small>Creater Id</small>
          </p>
        </Media>
        <Media right className="align-self-center">
          <Button
            color="link"
            className={`text-capitalize text-${statusMap[status]}`}
          >
           Likes
          </Button>
        </Media>
      </Media>
      <Media>
        <p className="text-muted">Question and Answers</p>
      </Media>
      <Media left className="align-self-center">
          <Button
            color="link"
            className={`text-capitalize text-${statusMap[status]}`}
          >
           Comments
          </Button>
        </Media>
             
              </CardBody>
            </Card>
          </Col>
             ),
             )} */}
        {/* <Col md={6} sm={6} xs={12} className="mb-3">
          <Card className="flex-row">
            <CardImg
              className="card-img-left"
              src={bg1Image}
              style={{ width: 'auto', height: 150 }}
            />
            <CardBody>
              <CardTitle>Horizontal Image Card(Left)</CardTitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
            </CardBody>
          </Card>
        </Col>

        <Col md={6} sm={6} xs={12} className="mb-3">
          <Card className="flex-row">
            <CardImg
              className="card-img-left"
              src={bg1Image}
              style={{ width: 'auto', height: 150 }}
            />
            <CardBody>
              <CardTitle>Horizontal Image Card(Left)</CardTitle>
              <CardText>
                Some quick example text to build on the card title and make up
                the bulk of the card's content.
              </CardText>
            </CardBody>
          </Card>
        </Col> */}
    
      {/* <Row>
        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Input Types</CardHeader>
            <CardBody>
              <Form>
                <FormGroup>
                  <Label for="exampleEmail">Plain Text (Static)</Label>
                  <Input
                    plaintext
                    value="Some plain text/ static value"
                    readOnly
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleEmail">Email</Label>
                  <Input
                    type="email"
                    name="email"
                    placeholder="with a placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="examplePassword">Password</Label>
                  <Input
                    type="password"
                    name="password"
                    placeholder="password placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleUrl">Url</Label>
                  <Input
                    type="url"
                    name="url"
                    id="exampleUrl"
                    placeholder="url placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleNumber">Number</Label>
                  <Input
                    type="number"
                    name="number"
                    id="exampleNumber"
                    placeholder="number placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleDatetime">Datetime</Label>
                  <Input
                    type="datetime"
                    name="datetime"
                    id="exampleDatetime"
                    placeholder="datetime placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleDate">Date</Label>
                  <Input
                    type="date"
                    name="date"
                    id="exampleDate"
                    placeholder="date placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleTime">Time</Label>
                  <Input
                    type="time"
                    name="time"
                    id="exampleTime"
                    placeholder="time placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleColor">Color</Label>
                  <Input
                    type="color"
                    name="color"
                    id="exampleColor"
                    placeholder="color placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleSearch">Search</Label>
                  <Input
                    type="search"
                    name="search"
                    id="exampleSearch"
                    placeholder="search placeholder"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleSelect">Select</Label>
                  <Input type="select" name="select">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="exampleSelectMulti">Select Multiple</Label>
                  <Input type="select" name="selectMulti" multiple>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </Input>
                </FormGroup>
                <FormGroup>
                  <Label for="exampleText">Text Area</Label>
                  <Input type="textarea" name="text" />
                </FormGroup>
                <FormGroup>
                  <Label for="exampleFile">File</Label>
                  <Input type="file" name="file" />
                  <FormText color="muted">
                    This is some placeholder block-level help text for the above
                    input. It's a bit lighter and easily wraps to a new line.
                  </FormText>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="radio" /> Option one is this and that—be sure
                    to include why it's great
                  </Label>
                </FormGroup>
                <FormGroup check>
                  <Label check>
                    <Input type="checkbox" /> Check me out
                  </Label>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>

        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Form Grid</CardHeader>
            <CardBody>
              <Form>
                <FormGroup row>
                  <Label for="exampleEmail" sm={2}>
                    Email
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="email"
                      name="email"
                      placeholder="with a placeholder"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="examplePassword" sm={2}>
                    Password
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="password"
                      name="password"
                      placeholder="password placeholder"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleSelect" sm={2}>
                    Select
                  </Label>
                  <Col sm={10}>
                    <Input type="select" name="select" />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleSelectMulti" sm={2}>
                    Select Multiple
                  </Label>
                  <Col sm={10}>
                    <Input type="select" name="selectMulti" multiple />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleText" sm={2}>
                    Text Area
                  </Label>
                  <Col sm={10}>
                    <Input type="textarea" name="text" />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleFile" sm={2}>
                    File
                  </Label>
                  <Col sm={10}>
                    <Input type="file" name="file" />
                    <FormText color="muted">
                      This is some placeholder block-level help text for the
                      above input. It's a bit lighter and easily wraps to a new
                      line.
                    </FormText>
                  </Col>
                </FormGroup>
                <FormGroup tag="fieldset" row>
                  <Label for="checkbox2" sm={2}>
                    Radio
                  </Label>
                  <Col sm={10}>
                    <FormGroup check>
                      <Label check>
                        <Input type="radio" name="radio2" /> Option one is this
                        and that—be sure to include why it's great
                      </Label>
                    </FormGroup>
                    <FormGroup check>
                      <Label check>
                        <Input type="radio" name="radio2" /> Option two can be
                        something else and selecting it will deselect option one
                      </Label>
                    </FormGroup>
                    <FormGroup check disabled>
                      <Label check>
                        <Input type="radio" name="radio2" disabled /> Option
                        three is disabled
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="checkbox2" sm={2}>
                    Checkbox
                  </Label>
                  <Col sm={{ size: 10 }}>
                    <FormGroup check>
                      <Label check>
                        <Input type="checkbox" id="checkbox2" /> Check me out
                      </Label>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup check row>
                  <Col sm={{ size: 10, offset: 2 }}>
                    <Button>Submit</Button>
                  </Col>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Form Validation</CardHeader>
            <CardBody>
              <Form>
                <FormGroup>
                  <Label for="exampleEmail">Input with success</Label>
                  <Input valid />
                  <FormFeedback>
                    <a href="https://github.com/twbs/bootstrap/issues/23372">
                      A bug
                    </a>{' '}
                    fixed in (the currently unreleased) (
                    <a href="https://github.com/twbs/bootstrap/pull/23377">
                      PR
                    </a>
                    ) bootstrap{' '}
                    <a href="https://github.com/twbs/bootstrap/issues/23278">
                      v4 beta-2
                    </a>{' '}
                    shows invalid-feedback with is-valid inputs.
                  </FormFeedback>
                  <FormText>Example help text that remains unchanged.</FormText>
                </FormGroup>
                <FormGroup>
                  <Label for="examplePassword">Input with danger</Label>
                  <Input valid={false} />
                  <FormFeedback>
                    Oh noes! that name is already taken
                  </FormFeedback>
                  <FormText>Example help text that remains unchanged.</FormText>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>

        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Hidden Labels</CardHeader>
            <CardBody>
              <Form inline>
                <FormGroup>
                  <Label for="exampleEmail" hidden>
                    Email
                  </Label>
                  <Input type="email" name="email" placeholder="Email" />
                </FormGroup>{' '}
                <FormGroup>
                  <Label for="examplePassword" hidden>
                    Password
                  </Label>
                  <Input
                    type="password"
                    name="password"
                    placeholder="Password"
                  />
                </FormGroup>{' '}
                <Button>Submit</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Inline Form</CardHeader>
            <CardBody>
              <Form inline>
                <FormGroup>
                  <Label for="exampleEmail">Email</Label>{' '}
                  <Input
                    type="email"
                    name="email"
                    placeholder="something@idk.cool"
                  />
                </FormGroup>{' '}
                <FormGroup>
                  <Label for="examplePassword">Password</Label>{' '}
                  <Input
                    type="password"
                    name="password"
                    placeholder="don't tell!"
                  />
                </FormGroup>{' '}
                <Button>Submit</Button>
              </Form>
            </CardBody>
          </Card>
        </Col>

        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Inline Checkboxes</CardHeader>
            <CardBody>
              <Form>
                <FormGroup check inline>
                  <Label check>
                    <Input type="checkbox" /> Some input
                  </Label>
                </FormGroup>
                <FormGroup check inline>
                  <Label check>
                    <Input type="checkbox" /> Some other input
                  </Label>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row>

      <Row>
        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Input Sizing</CardHeader>
            <CardBody>
              <Form>
                <Input className="mb-2" placeholder="lg" bsSize="lg" />
                <Input className="mb-2" placeholder="default" />
                <Input className="mb-2" placeholder="sm" bsSize="sm" />
                <Input className="mb-2" type="select" bsSize="lg">
                  <option>Large Select</option>
                </Input>
                <Input className="mb-2" type="select">
                  <option>Default Select</option>
                </Input>
                <Input className="mb-2" type="select" bsSize="sm">
                  <option>Small Select</option>
                </Input>
              </Form>
            </CardBody>
          </Card>
        </Col>

        <Col xl={6} lg={12} md={12}>
          <Card>
            <CardHeader>Input Grid Sizing</CardHeader>
            <CardBody>
              <Form>
                <FormGroup row>
                  <Label for="exampleEmail" sm={2} size="lg">
                    Email
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="email"
                      name="email"
                      placeholder="lg"
                      bsSize="lg"
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="exampleEmail2" sm={2}>
                    Email
                  </Label>
                  <Col sm={10}>
                    <Input
                      type="email"
                      name="email"
                      id="exampleEmail2"
                      placeholder="default"
                    />
                  </Col>
                </FormGroup>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </Row> */}
    </Page>
  );
}
};

export default ForumPage;
