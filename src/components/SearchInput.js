import React from 'react';
import { MdSearch } from 'react-icons/md';
import { Form, Input } from 'reactstrap';




class SearchInput extends React.Component {
  constructor(props) {
    super(props);
 
    this.state = {
      searchString: '' 
    };
  }
  handleChange = (e) => {
    this.setState({ searchString:e.target.value });
  }
  render(){
    var libraries = [

      { name: 'Backbone.js', url: 'https://documentcloud.github.io/backbone/'},
      { name: 'AngularJS', url: 'https://angularjs.org/'},
      { name: 'jQuery', url: 'https://jquery.com/'},
      { name: 'Prototype', url: 'http://www.prototypejs.org/'},
      { name: 'React', url: 'https://facebook.github.io/react/'},
      { name: 'Ember', url: 'http://emberjs.com/'},
      { name: 'Knockout.js', url: 'https://knockoutjs.com/'},
      { name: 'Dojo', url: 'http://dojotoolkit.org/'},
      { name: 'Mootools', url: 'http://mootools.net/'},
      { name: 'Underscore', url: 'https://documentcloud.github.io/underscore/'},
      { name: 'Lodash', url: 'http://lodash.com/'},
      { name: 'Moment', url: 'https://momentjs.com/'},
      { name: 'Express', url: 'http://expressjs.com/'},
      { name: 'Koa', url: 'http://koajs.com/'},
    
    ];
    var  searchString = this.state.searchString.trim().toLowerCase();
         console.log(libraries);
     if (searchString.length > 0) {
       console.log(searchString)
       libraries = libraries.filter(function(i) {
         console.log(i);
         return i.name.toLowerCase().match( searchString );
       });
     }
   
  return (
    <Form inline className="cr-search-form" onSubmit={e => e.preventDefault()}>
      <MdSearch
        size="20"
        className="cr-search-form__icon-search text-secondary"
      />
      <Input
      value={this.state.searchString}
       onChange={this.handleChange}
        type="search"
        className="cr-search-form__input"
        placeholder="Search..."
      />
    
            {/* {libraries.map(function(i) {
                return <li>{i.name} <a href={i.url}>{i.url}</a></li>;
            }) }  */}
       
          
    </Form>
  );
};
}

export default SearchInput;
